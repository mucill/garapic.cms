-- --------------------------------------------------------
-- Host:                         localhost
-- Versi server:                 10.1.9-MariaDB-log - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table mgi.cms_translate: 27 rows
/*!40000 ALTER TABLE `cms_translate` DISABLE KEYS */;
INSERT INTO `cms_translate` (`id`, `slug`, `updated_at`) VALUES
	(1, 'home', '2016-05-24 22:09:59'),
	(2, 'sitemap', '2016-05-24 22:12:18'),
	(3, 'registration', '2016-05-24 22:12:44'),
	(4, 'member_login', '2016-05-24 22:13:21'),
	(5, 'contact', '2016-05-24 22:13:45'),
	(6, 'call_us', '2016-05-24 22:14:20'),
	(7, 'news', '2016-05-24 22:14:35'),
	(8, 'information', '2016-05-24 22:15:01'),
	(9, 'links', '2016-05-24 22:15:44'),
	(10, 'announcement', '2016-05-24 22:16:01'),
	(11, 'gallery', '2016-05-24 22:16:14'),
	(12, 'stats', '2016-05-24 22:16:42'),
	(13, 'maintenance_by', '2016-05-24 22:17:31'),
	(14, 'visitor', '2016-05-24 22:22:13'),
	(15, 'membership', '2016-05-25 06:05:06'),
	(16, 'membership_info', '2016-05-25 06:08:32'),
	(17, 'detail', '2016-05-25 06:09:23'),
	(18, 'news_info', '2016-05-25 06:10:39'),
	(19, 'share_title', '2016-05-25 06:11:53'),
	(20, 'share_info', '2016-05-25 06:14:56'),
	(21, 'registration_info', '2016-05-25 06:16:59'),
	(22, 'event', '2016-05-25 06:37:56'),
	(23, 'summary', '2016-05-25 06:54:53'),
	(24, 'blank', '2016-05-25 06:59:15'),
	(25, 'archives', '2016-05-25 10:23:17'),
	(26, 'news_populer', '2016-05-25 10:25:22'),
	(27, 'news_latest', '2016-05-25 10:24:43');
/*!40000 ALTER TABLE `cms_translate` ENABLE KEYS */;

-- Dumping data for table mgi.cms_translate_lang: 54 rows
/*!40000 ALTER TABLE `cms_translate_lang` DISABLE KEYS */;
INSERT INTO `cms_translate_lang` (`id`, `lang`, `translate`, `updated_at`) VALUES
	(1, 'eng', 'Home', '2016-05-24 22:09:59'),
	(1, 'id', 'Beranda', '2016-05-24 22:09:59'),
	(2, 'eng', 'Sitemap', '2016-05-24 22:12:18'),
	(2, 'id', 'Peta Situs', '2016-05-24 22:12:18'),
	(3, 'eng', 'Registration', '2016-05-24 22:12:44'),
	(3, 'id', 'Pendaftaran', '2016-05-24 22:12:44'),
	(4, 'eng', 'Member Login', '2016-05-24 22:13:21'),
	(4, 'id', 'Login Anggota', '2016-05-24 22:13:21'),
	(5, 'eng', 'Contact', '2016-05-24 22:13:45'),
	(5, 'id', 'Hubungi', '2016-05-24 22:13:45'),
	(6, 'eng', 'Call Us', '2016-05-24 22:14:20'),
	(6, 'id', 'Sapa Kami', '2016-05-24 22:14:20'),
	(7, 'eng', 'News', '2016-05-24 22:14:35'),
	(7, 'id', 'Berita', '2016-05-24 22:14:35'),
	(8, 'eng', 'Information', '2016-05-24 22:15:01'),
	(8, 'id', 'Informasi', '2016-05-24 22:15:01'),
	(9, 'eng', 'Other Link', '2016-05-24 22:15:44'),
	(9, 'id', 'Situs Terkait', '2016-05-24 22:15:44'),
	(10, 'eng', 'Announcement', '2016-05-24 22:16:02'),
	(10, 'id', 'Pengumuman', '2016-05-24 22:16:01'),
	(11, 'eng', 'Gallery', '2016-05-24 22:16:14'),
	(11, 'id', 'Galeri', '2016-05-24 22:16:14'),
	(12, 'eng', 'Statistic', '2016-05-24 22:16:42'),
	(12, 'id', 'Statistik', '2016-05-24 22:16:42'),
	(13, 'eng', 'Maintenance By', '2016-05-24 22:17:31'),
	(13, 'id', 'Dikelola Oleh', '2016-05-24 22:17:31'),
	(14, 'eng', 'Visitor', '2016-05-24 22:22:14'),
	(14, 'id', 'Kunjungan', '2016-05-24 22:22:14'),
	(15, 'eng', 'Membership', '2016-05-25 06:05:06'),
	(15, 'id', 'Keanggotaan', '2016-05-25 06:05:06'),
	(16, 'eng', 'Get in touch with fellow member with ease.', '2016-05-25 06:08:33'),
	(16, 'id', 'Keanggotaan IGI akan memudahkan saling berinteraks', '2016-05-25 06:08:33'),
	(17, 'eng', 'Detail', '2016-05-25 06:09:23'),
	(17, 'id', 'Selengkapnya', '2016-05-25 06:09:23'),
	(18, 'eng', 'Get the latest news arround Geografi World.', '2016-05-25 06:10:39'),
	(18, 'id', 'Dapatkan berita-berita terbaru ter-update seputar ', '2016-05-25 06:10:39'),
	(19, 'eng', 'Informarmation Sharing', '2016-05-25 06:11:53'),
	(19, 'id', 'Berbagi Informasi', '2016-05-25 06:11:53'),
	(20, 'eng', 'Meet the Geography experts around Indonesia.', '2016-05-25 06:14:56'),
	(20, 'id', 'Bergabunglah dengan para pakar Geografi dari selur', '2016-05-25 06:14:56'),
	(21, 'eng', 'I want to register as a member of the Association ', '2016-05-25 06:16:59'),
	(21, 'id', 'Saya ingin mendaftar sebagai anggota Ikatan Geogra', '2016-05-25 06:16:59'),
	(22, 'eng', 'Event', '2016-05-25 06:37:56'),
	(22, 'id', 'Agenda Kegiatan', '2016-05-25 06:37:56'),
	(23, 'eng', 'Summary', '2016-05-25 06:54:53'),
	(23, 'id', 'Ringkasan', '2016-05-25 06:54:53'),
	(24, 'eng', 'Try another page or back to <a href="../">home</a>', '2016-05-25 07:09:03'),
	(24, 'id', 'Silahkan kunjungi laman lainnya atau kembali ke <a href="../">beranda</a>', '2016-05-25 07:09:03'),
	(25, 'id', 'Arsip Berita', '2016-05-25 10:23:17'),
	(25, 'eng', 'Archives', '2016-05-25 10:23:17'),
	(26, 'id', 'Terpopuler', '2016-05-25 10:24:24'),
	(26, 'eng', 'Most Read', '2016-05-25 10:24:24'),
	(27, 'id', 'Terbaru', '2016-05-25 10:24:43'),
	(27, 'eng', 'Latest News', '2016-05-25 10:24:43');
/*!40000 ALTER TABLE `cms_translate_lang` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

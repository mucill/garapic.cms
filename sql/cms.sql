-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';

DROP TABLE IF EXISTS `cms_category`;
CREATE TABLE `cms_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT 'post',
  `is_default` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_category_lang`;
CREATE TABLE `cms_category_lang` (
  `lang` varchar(45) NOT NULL,
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL,
  KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


SET NAMES utf8mb4;

DROP TABLE IF EXISTS `cms_config`;
CREATE TABLE `cms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `label` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `groups` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cms_districts`;
CREATE TABLE `cms_districts` (
  `id` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `regency_id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `districts_regency_id_index` (`regency_id`),
  CONSTRAINT `districts_regency_id_foreign` FOREIGN KEY (`regency_id`) REFERENCES `cms_regencies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_event`;
CREATE TABLE `cms_event` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `users_id` int(10) DEFAULT NULL,
  `status` enum('draft','publish') DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_users1_idx` (`users_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_event_lang`;
CREATE TABLE `cms_event_lang` (
  `lang` varchar(5) NOT NULL,
  `id` int(10) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` text,
  `place` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`,`lang`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_groups`;
CREATE TABLE `cms_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cms_lang`;
CREATE TABLE `cms_lang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shortname` varchar(5) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `is_default` int(1) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_logs`;
CREATE TABLE `cms_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_id` int(11) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `log_flag` (`flag`(250)),
  KEY `log_person_id` (`users_id`),
  FULLTEXT KEY `log_message` (`message`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cms_media`;
CREATE TABLE `cms_media` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `type` varchar(20) DEFAULT 'jpg',
  `size` decimal(10,0) DEFAULT '0',
  `alias` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `download` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_media_lang`;
CREATE TABLE `cms_media_lang` (
  `lang` varchar(5) NOT NULL,
  `id` int(10) NOT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`lang`,`id`),
  KEY `fk_media_lang_media1_idx` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_member`;
CREATE TABLE `cms_member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `realname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birth_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_addr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_city` int(10) DEFAULT NULL,
  `home_prov` int(10) DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ahli_bid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `socmed_fb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `socmed_tw` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `reg_date` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_member_abdi`;
CREATE TABLE `cms_member_abdi` (
  `abdi_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `abdi_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abdi_partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abdi_year` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abdi_stake` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`abdi_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_member_edu`;
CREATE TABLE `cms_member_edu` (
  `edu_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `sarjana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jurusan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kampus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lulus` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skripsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`edu_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_member_jabatan`;
CREATE TABLE `cms_member_jabatan` (
  `jabatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `jbt_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jbt_office` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jbt_periode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`jabatan_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_member_office`;
CREATE TABLE `cms_member_office` (
  `office_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `office_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_addr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_prov` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`office_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_member_publikasi`;
CREATE TABLE `cms_member_publikasi` (
  `pub_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `pub_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub_partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub_isbn` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub_year` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub_abstract` text COLLATE utf8_unicode_ci,
  `pub_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`pub_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_member_riset`;
CREATE TABLE `cms_member_riset` (
  `riset_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `riset_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `riset_partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `riset_year` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `riset_fund` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `riset_stake` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`riset_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_member_tokens`;
CREATE TABLE `cms_member_tokens` (
  `token_id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) CHARACTER SET utf8 NOT NULL,
  `member_id` int(11) NOT NULL,
  `created` date DEFAULT NULL,
  PRIMARY KEY (`token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_message`;
CREATE TABLE `cms_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `from` varchar(100) DEFAULT NULL,
  `to` varchar(100) DEFAULT NULL,
  `subject` text,
  `message` text,
  `status` int(1) DEFAULT '0' COMMENT '0 - New 1 - Read 9 Trash',
  `reply` bigint(20) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_modules`;
CREATE TABLE `cms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(2) NOT NULL DEFAULT '0',
  `show` int(1) NOT NULL DEFAULT '1',
  `path` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_write` int(1) DEFAULT NULL COMMENT 'Has add method ? 1=Yes 0=No',
  `has_update` int(1) DEFAULT NULL COMMENT 'Has update method ? 1=Yes 0=No',
  `has_delete` int(1) DEFAULT NULL COMMENT 'Has delete method ? 1=Yes 0=No',
  `orders` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `cms_nav`;
CREATE TABLE `cms_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_nav_groups`;
CREATE TABLE `cms_nav_groups` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `slug` varchar(50) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_nav_lang`;
CREATE TABLE `cms_nav_lang` (
  `lang` varchar(5) NOT NULL,
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`lang`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_page`;
CREATE TABLE `cms_page` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `users_id` int(10) NOT NULL,
  `type` varchar(50) DEFAULT 'post',
  `status` enum('draft','publish') DEFAULT 'draft',
  `media_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_page_category_idx` (`category_id`),
  KEY `fk_page_users1_idx` (`users_id`),
  KEY `fk_page_media1_idx` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_page_lang`;
CREATE TABLE `cms_page_lang` (
  `lang` varchar(5) NOT NULL,
  `id` int(10) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `intro` text,
  `body` text,
  `slug` varchar(200) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  PRIMARY KEY (`lang`,`id`),
  KEY `fk_page_lang_page1_idx` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_priviledges`;
CREATE TABLE `cms_priviledges` (
  `modules_id` int(2) NOT NULL,
  `groups_id` int(2) NOT NULL,
  `can_write` int(1) NOT NULL DEFAULT '0' COMMENT '0 = No - 1 = True',
  `can_update` int(1) NOT NULL DEFAULT '0' COMMENT '0 = No - 1 = True',
  `can_delete` int(1) NOT NULL DEFAULT '0' COMMENT '0 = No - 1 = True',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cms_provinces`;
CREATE TABLE `cms_provinces` (
  `id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_regencies`;
CREATE TABLE `cms_regencies` (
  `id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regencies_province_id_index` (`province_id`),
  CONSTRAINT `regencies_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `cms_provinces` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_sessions`;
CREATE TABLE `cms_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_stats`;
CREATE TABLE `cms_stats` (
  `sessid` varchar(35) NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sessid`),
  UNIQUE KEY `sessid` (`sessid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_translate`;
CREATE TABLE `cms_translate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_translate_lang`;
CREATE TABLE `cms_translate_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(50) NOT NULL,
  `translate` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `cms_users`;
CREATE TABLE `cms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) CHARACTER SET utf8 NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `activation_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `forgotten_password_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cms_users_groups`;
CREATE TABLE `cms_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `cms_villages`;
CREATE TABLE `cms_villages` (
  `id` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `district_id` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `villages_district_id_index` (`district_id`),
  CONSTRAINT `villages_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `cms_districts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `cms_widget`;
CREATE TABLE `cms_widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `content` longtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2016-08-16 01:01:17

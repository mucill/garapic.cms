<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function _widget_nav_uri($type, $url) 
{
    $CI         =& get_instance();
    switch($type) {
        case 'module':
            $nav_uri = site_url($url);
            break;

        case 'web':
            $nav_uri = $url;
            break;        

        default:
            $sql =  'SELECT b.slug
                     FROM cms_page a
                     LEFT JOIN cms_page_lang b ON b.id = a.id
                     WHERE b.id = "'.$url.'"';
            $query = $CI->db->query($sql);
            $nav = $query->row();
            $nav_uri = site_url('read/'.$nav->slug);
            break;        
    }

    return $nav_uri;
}

function widget_nav($groups = null)
{
    $CI         =& get_instance();
    $sql        =   'SELECT
                        a.id id, c.name name, a.slug slug, a.type type 
                     FROM cms_nav a
                     LEFT JOIN cms_nav_groups b ON b.id = a.nav_id
                     LEFT JOIN cms_nav_lang c ON c.id = a.id
                     WHERE b.slug = "'.$groups.'"
                     AND c.lang = "'.$CI->session->userdata('lang').'"
                     AND a.parent_id = 0';
    $query      = $CI->db->query($sql);
    $nav  = '<ul>';
    foreach($query->result() as $navs){
        $sql_child  =   'SELECT
                            c.name name, a.slug slug, a.type type 
                         FROM cms_nav a
                         LEFT JOIN cms_nav_groups b ON b.id = a.nav_id
                         LEFT JOIN cms_nav_lang c ON c.id = a.id
                         WHERE a.parent_id = "'.$navs->id.'"
                         AND c.lang = "'.$CI->session->userdata('lang').'"';
        $query      = $CI->db->query($sql_child);
        if($query->num_rows() > 0) {
            $nav .= '<li><a href="'._widget_nav_uri($navs->type, $navs->slug).'"><div>'.$navs->name.'</div></a>';
            $nav .= '<ul>';            
            foreach($query->result() as $navs_child){
                $nav .= '<li><a href="'._widget_nav_uri($navs_child->type, $navs_child->slug).'"><div>'.$navs_child->name.'</div></a></li>';
            }
            $nav .= '</ul>';            
        } else {
            $nav .= '<li><a href="'._widget_nav_uri($navs->type, $navs->slug).'"><div>'.$navs->name.'</div></a></li>';
        }
    }
    $nav  .= '</ul>';
    echo $nav;
}

function widget_message_unread()
{
    $CI         =& get_instance();
    // show unread counter
    $current_user           = $CI->session->userdata('member_id');
    $unread = 0;
    $CI->db
    ->where('to', $current_user)
    ->where('status', 0);
    $query = $CI->db->get('cms_message');
    if($query->num_rows() > 0) {
        $unread = $query->num_rows();            
    }

    return $unread;

}

function widget_media($folder = null, $thumb = false)
{
    $CI         =& get_instance();
    $media      = '';
    // get current id for selected folder
    $CI->db
    ->select('a.id')
    ->from('cms_category a')
    ->join('cms_category_lang b','b.id = a.id','left')
    ->where('a.type', 'folder')
    ->where('b.slug', $folder)
    ->limit(1);
    $query = $CI->db->get();
    if($query->num_rows() > 0 ) {
        $category = $query->row();
        // debug($CI->db->last_query());


        // get media
        $CI->db
        ->from('cms_media a')
        ->join('cms_media_lang b', 'b.id = a.id')
        ->where('a.category_id', $category->id)
        ->where('b.lang', $CI->session->userdata('lang'));
        $query = $CI->db->get();    
        if($query->num_rows() > 0 ) {
            if($thumb) {
                $media = '<div class="masonry-thumbs col-3" data-lightbox="gallery">';
                foreach($query->result() as $file) {
                    $media .= '<a href="'.site_url('media/') .'/'. $CI->media->images($file->id) . '" data-lightbox="gallery-item"><img class="image_fade" style="height:90px !important; overflow:hidden;" src="'.site_url('media/') .'/'. $CI->media->images($file->id).'" alt="'.$file->alt.'"></a>';
                }                
                $media .= '</div>';
            } else {            
                $media = '<ul>';
                foreach($query->result() as $file) {
                    $media .= '<li><a href="'.base_url(MEDIA . '/'. $file->filename).'">' . (($file->alt == '') ? $file->filename : $file->alt) .'</a></li>';
                }
                $media .= '</ul>';
            }
        }
    }
    echo $media; 
}

function widget_current_language()
{
    $CI         =& get_instance();
    echo $CI->session->userdata('lang');
}

function __($slug = '', $print = true) 
{
    $CI         =& get_instance();
    $CI->db
    ->select('b.translate')
    ->from('cms_translate a')
    ->join('cms_translate_lang b', 'b.id = a.id', 'left')
    ->where('a.slug', $slug)
    ->where('b.lang', $CI->session->userdata('lang'));
    $query      = $CI->db->get('cms_translate');
    if($query->num_rows() > 0) {
        $t = $query->row();
        if($print) {
            echo $t->translate;         
        } else {
            return $t->translate;;
        }
    } else {
        echo '';
    }
}

function widget_debug($string) 
{
    echo '<pre>';
    print_r($string);
    echo '</pre>';
    exit;
}

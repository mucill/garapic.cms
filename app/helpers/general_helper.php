<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function get_conf($name = '')
{
	$CI 		=& get_instance();
	$sql 		= "SELECT value FROM cms_config WHERE name='$name'";
	$query 		= $CI->db->query($sql);	 
	if ($query->num_rows() > 0) {
		$setting	= $query->row();
		return $setting->value;
	}
}

function get_remote_url()
{
	$remote_url 		= get_conf('site_url');
	return $remote_url;
}

function get_remote_path()
{
	$resource_path 		= MEDIA;
	return get_remote_url().$resource_path;
}

function get_user_counter()
{
	// Define CI
	$CI 		=& get_instance();
	$count 		= $CI->db->count_all('cms_stats');

	if($count > 0) {
		return $count; 
	} else { 
		return 0; 
	} 
}

function get_user_online()
{
	// Define CI
	$CI 		=& get_instance();

	/* Define how long the maximum amount of time the session can be inactive. */ 
	define("MAX_IDLE_TIME", 3); 

	if ( $directory_handle = opendir( session_save_path() ) ) { 
		$count = 0; 
		while ( false !== ( $file = readdir( $directory_handle ) ) ) { 
			if($file != '.' && $file != '..'){ 
				// Comment the 'if(...){' and '}' lines if you get a significant amount of traffic 
				if(time()- fileatime(session_save_path() . '/' . $file) < MAX_IDLE_TIME * 60) { 
					$count++; 
				} 
			} 
		}
		$sql = "INSERT IGNORE INTO cms_stats(`sessid`) VALUES ('".session_id()."')  ";			
		$CI->db->query($sql);	 
		closedir($directory_handle); 
		return $count; 
	} else { 
		return false; 
	} 
}

function get_widget($slug = null)
{
    // ob_start();
    // $content    = ob_get_clean();
    $CI             =& get_instance();
    $content        = '';
    $cache_filename = APPPATH . 'cache' .DIRECTORY_SEPARATOR . $CI->session->session_id.'.php';

    $CI->db
    ->where('slug', $slug)
    ->limit(1);

    $query = $CI->db->get('cms_widget');    

    if($query->num_rows() > 0 ) {
        $widget = $query->row();
        $content = $widget->content; 
    }

    write_file($cache_filename, $content);
    return $CI->load->ext_view($cache_filename);
}

function get_gallery($cat_id, $limit) 
{
    $CI         =& get_instance();
    $query = $CI->db->where('media_lang.lang', $CI->session->userdata('lang'))
    ->where('media.category_id', $cat_id)
    ->join('media_lang', 'media_lang.id = media.id')
    ->limit($limit)
    ->get('media');
    $data = $query->result();
    if($query->num_rows() > 0) {
        foreach ($query->result() as $pic) {
            echo '
            <a href="'.site_url('media') .'/'. $pic->alias .'" data-lightbox="gallery-item">
            <img class="image_fade" style="height:90px !important; overflow:hidden;" src="'.site_url('media') .'/'. $pic->alias . '" alt="'.$pic->alt.'">
            </a>';
        }
    } else {
        return null;
    }
}

function debug($string) 
{
	echo '<pre>';
	print_r($string);
	echo '</pre>';
	exit;
}

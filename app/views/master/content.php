<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<title><?php echo get_conf('site_name') ?></title>
<meta charset="utf-8" />    
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="<?php echo get_conf('site_tagline') ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="<?php echo get_conf('meta_description') ?>"> 
<meta name="robots" content="index, follow">
<link rel="icon" type="image/x-icon" href="<?php echo base_url('favicon.ico') ?>" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>bootstrap.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>style.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>animate.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>magnific-popup.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>responsive.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>dark.css" />

<link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>ionicons-min/css/ionicons.min.css" />
<link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>chosen/chosen.min.css" />

<link rel="stylesheet" href="<?php echo base_url() . CSS ?>custom.css" />
<!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo base_url() . JS ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url() . PLUGINS ?>chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() . JS ?>plugins.js"></script>
</head>
<body>

    <div id="wrapper" class="clearfix">
        <?php $this->load->view('master/nav') ?>

        <?php if($this->uri->segment(1) != '') : ?>
        <section id="page-title" class="nomargin page-title-dark" style="background: transparent url(<?php echo base_url() . UPLOADS ?>/slider.jpg) center center no-repeat; background-size: cover;">
            <div class="container clearfix">
                <h1><?php echo $title ?></h1>
            </div>
        </section>
        <?php endif ?>
        
        <?php if(! $this->uri->segment(1)) $this->load->view('slider') ?>

        <section id="content">
            <div class="content-wrap">
            <?php echo $content ?>                
            </div>
        </section>


        <footer id="footer" class="dark noborder" style="background: url('../assets/images/footer-bg.jpg') center top repeat; background-size: 100% 100%">
            <div class="container">
                <?php get_widget('widget-1') ?>
            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        &copy; <?php echo get_conf('site_tagline') ?> 2015 - <?php echo date('Y') ?> <br><?php __('share') ?>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <?php __('maintenance_by') ?><br><?php echo get_conf('site_tagline') ?>
                        </div>
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->
    </div> 

    <div id="gotoTop" class="icon-angle-up"></div>
    <script type="text/javascript" src="<?php echo base_url('assets/js/functions.js') ?>"></script>
</body>
</html>
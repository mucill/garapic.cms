<div id="top-bar">
    <div class="container clearfix">
        <div class="col_half nobottommargin">
            <div class="top-links">
                <ul>
                    <li><a href="<?php echo site_url() ?>"><?php __('home') ?></a></li>
                    <li><a href="<?php echo site_url(($this->session->userdata('lang') == 'id') ? 'read/peta-situs' : 'read/site-map' ) ?>"><?php __('sitemap') ?></a></li>
                    <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php widget_current_language() ?></a>
                        <ul class="dropdown-menu">
                            <?php foreach($this->media->avail_language() as $lang) : ?>
                            <li><a href="<?php echo site_url('langs/setup/'.$lang->shortname.'/'.str_replace('/','.',$this->uri->uri_string())) ?>"><?php echo $lang->shortname ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col_half col_last fright nobottommargin">
            <div class="top-links">
                <ul>
                    <?php if(isset($this->session->userdata['email'])) : ?>
                    <li><a href="#" style="color: lightblue; font-weight: normal !important; ">Hai, <?php echo $this->session->userdata('realname') ?></a></li>
                    <li><a href="<?php echo site_url('member/profile') ?>">Profil</a></li>
                    <li><a href="<?php echo site_url('message') ?>">Message <?php echo (widget_message_unread() > 0) ? '('.widget_message_unread().')' : '' ?></a></li>
                    <li><a href="<?php echo site_url('member/logout') ?>">Logout</a></li>
                    <?php else : ?>
                    <li><a href="<?php echo site_url('member/register') ?>"><?php __('registration') ?></a></li>
                    <li><a href="<?php echo site_url('member/login') ?>"><?php __('member_login') ?></a></li>
                    <?php endif ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<header id="header" class="sticky-style-2">
    <div class="container clearfix">
        <div id="logo">
            <a href="<?php echo site_url() ?>" class="standard-logo" data-dark-logo="<?php echo base_url('/assets')?>/img/logo-dark.png"><img src="<?php echo base_url('/assets')?>/img/logo.png" alt="<?php echo get_conf('site_name') ?>"></a>
            <a href="<?php echo site_url() ?>" class="retina-logo" data-dark-logo="<?php echo base_url('/assets')?>/img/logo-dark@2x.png"><img src="<?php echo base_url('/assets')?>/img/logo@2x.png" alt="<?php echo get_conf('site_name') ?>"></a>
        </div>
        <ul class="header-extras">
            <li>
                <i class="i-plain icon-call nomargin"></i>
                <div class="he-text">
                    <?php __('contact') ?>
                    <span><?php echo get_conf('phone') ?></span>
                </div>
            </li>
            <li>
                <i class="i-plain icon-email3 nomargin"></i>
                <div class="he-text">
                    <?php __('call_us') ?>
                    <span><?php echo get_conf('email') ?></span>
                </div>
            </li>
        </ul>
    </div>
    <div id="header-wrap">
        <nav id="primary-menu" class="style-2 center">
            <div class="container clearfix">
                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                <?php echo widget_nav('main') ?>
            </div>
        </nav>
    </div>
</header>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publikasi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('publikasi_model', 'crudMod'));
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('text','date','form','security','string','url'));
	}
	
	public function index()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
		} else {
			
			$data = $this->session->userdata;
			$data['Avatar'] = $this->crudMod->getAvatar($data);
			$data['memBio'] = $this->publikasi_model->getPub($data);
			
			$validasi = array(
				array(
					'field'			=> 'prof_pub_title',
					'label'			=> 'Judul Publikasi',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan judul publikasi anda.'
					),
				),
				array(
					'field'			=> 'prof_pub_type',
					'label'			=> 'Jenis Publikasi',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon pilih salah satu jenis publikasi anda.'
					),
				),
				array(
					'field'			=> 'prof_pub_year',
					'label'			=> 'Tahun Terbit',
					'rules'			=> 'required|numeric|min_length[4]|max_length[4]',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan tahun terbit publikasi anda.',
						'numeric'	=> 'Mohon isikan tahun terbit publikasi hanya dengan angka.',
						'min_length'=> 'Penulisan tahun harus 4 angka.',
						'max_length'=> 'Penulisan tahun harus 4 angka.'
					),
				),
			);
			
			$this->form_validation->set_rules($validasi);
			$data['custom_error']	= '';
			$data['success']		= '';
			
			if(isset($_POST['submit'])) {
			
				if ($this->form_validation->run() == FALSE)
				{
					// If Failed
					$data['custom_error'] 	= validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');

				} else {
					
					// If Success
					$clean = $this->security->xss_clean($this->input->post(NULL, TRUE));
					$this->publikasi_model->addPub($clean);
					$data['success'] = '<li><i class="icon-line-square-check"></i>Data anda berhasil disimpan.</li>';
					redirect(base_url('dashboard/profile'));
				}
			}
		
			$view['content'] 		= $this->load->view('publikasi', $data, TRUE);
			$view['footer_gallery']	= $this->media->images_list(4, 6);
			$view['blog']			= 'Members Private Page';
			$this->load->view('master/full', $view);
		}
	}
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'member/');
	}
}

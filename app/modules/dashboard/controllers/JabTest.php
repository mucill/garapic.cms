<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// http://learnjquerybootstrap.blogspot.co.id/2015/01/crud-create-read-update-delete-using-codeigniter-and-bootstrap.html

class JabTest extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('crudMod');
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('text','date','form','security','string','url'));
	}
	
	public function index()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
		} else {
			
			$data = $this->session->userdata;
			$data['Avatar'] = $this->crudMod->getAvatar($data);
			$data['memJab'] = $this->crudMod->getProf($data);
			
			$validasi = array(
				array(
					'field'			=> 'prof_jbt_type',
					'label'			=> 'Jenis Jabatan',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan jenis jabatan anda.'
					),
				),
				array(
					'field'			=> 'prof_jbt_offc',
					'label'			=> 'Institusi',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan institusi jabatan anda.'
					),
				),
				array(
					'field'			=> 'prof_jbt_year',
					'label'			=> 'Periode Jabatan',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan tahun periode jabatan anda.'
					),
				),
			);
			
			$this->form_validation->set_rules($validasi);
			$data['custom_error']	= '';
			$data['success']		= '';
			
			if(isset($_POST['submit'])) {
			
				if ($this->form_validation->run() == FALSE)
				{
					// If Failed
					$data['custom_error'] 	= validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');

				} else {
					
					// If Success
					$clean = $this->security->xss_clean($this->input->post(NULL, TRUE));
					$this->crudMod->addJab($clean);
					$data['success'] = '<li><i class="icon-line-square-check"></i>Data anda berhasil disimpan.</li>';
					redirect(base_url('dashboard/profile'));
				}
			}
		
			$view['content'] 		= $this->load->view('viewJabatan', $data, TRUE);
			$view['footer_gallery']	= $this->media->images_list(4, 6);
			$view['blog']			= 'Members Private Page';
			$this->load->view('master/full', $view);
		}
	}
	
	public function update()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
		} else {
			
			$data			= $this->session->userdata;
			$jabID			= $this->uri->segment(4);
			$data['Profil']	= $this->crudMod->getProf($data);
			$data['Avatar']	= $this->crudMod->getAvatar($data);
			$data['memJab']	= $this->crudMod->getJabID($jabID);
			
			$validasi = array(
				array(
					'field'			=> 'prof_jbt_type',
					'label'			=> 'Jenis Jabatan',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan jenis jabatan anda.'
					),
				),
				array(
					'field'			=> 'prof_jbt_offc',
					'label'			=> 'Institusi',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan institusi jabatan anda.'
					),
				),
				array(
					'field'			=> 'prof_jbt_year',
					'label'			=> 'Periode Jabatan',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan tahun periode jabatan anda.'
					),
				),
			);
			
			$this->form_validation->set_rules($validasi);
			$data['custom_error']	= '';
			$data['success']		= '';
			
			if(isset($_POST['update'])) {
			
				if ($this->form_validation->run() == FALSE)
				{
					// If Failed
					$data['custom_error'] 	= validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');

				} else {
					
					$member_id		= $this->input->post('member_id');
					$jbt_type		= $this->input->post('prof_jbt_type');
					$jbt_office		= $this->input->post('prof_jbt_offc');
					$jbt_periode	= $this->input->post('prof_jbt_year');
					$last_update	= date('Y-m-d H:i:s');					

					$data = array(
						'member_id'		=> $member_id,
						'jbt_type'		=> $jbt_type,
						'jbt_office'	=> $jbt_office,
						'jbt_periode'	=> $jbt_periode,
						'last_update'	=> $last_update
					);
					
					$this->crudMod->updJab($jabID, $data);
					$data['success'] = '<li><i class="icon-line-square-check"></i>Data anda berhasil disimpan.</li>';
					redirect(base_url('dashboard/profile'));
				}
			}
		
			$view['content'] 		= $this->load->view('viewJabatan', $data, TRUE);
			$view['footer_gallery']	= $this->media->images_list(4, 6);
			$view['blog']			= 'Members Private Page';
			$this->load->view('master/full', $view);
		}
	}
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'member/');
	}
}

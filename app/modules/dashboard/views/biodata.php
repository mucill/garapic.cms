<section class="container clearfix">
	<div class="row">
		<div class="panel panel-default noshadow">
			<div class="panel-heading nobottompadding mbrMenu">
				<ul class="nav nav-tabs">
					<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/profile">Profile</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/biodata">Biodata</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/instansi">Instansi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pendidikan">Pendidikan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/jabatan">Jabatan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/publikasi">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/penelitian">Riset</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pengabdian">Pengabdian</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/logout">Logout</a></li>
				</ul>
			</div>
			<div class="panel-body norightpadding noleftpadding">
				<div class="tab-content">
					<div class="col-md-4 panel nobottommargin">
<?php $this->load->view('avatar'); ?>
					</div>
					<div class="col-md-8 panel nobottommargin">
<?php foreach ($getProf as $prof): ?>
						<!-- FORM BIODATA BEGIN -->
						<?php echo form_open(base_url('dashboard/biodata/'), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
							<?php if($custom_error == '') : ?>
							<h4 class="nomargin">Biodata Anggota</h4>
							Mohon lengkapi formulir biodata anggota berikut ini.
							<hr />
							<?php else: ?>
								<?php $this->load->view('submit_error') ?>
							<?php endif; ?>
							<?php if($success == '') : ?>
							<?php else: ?>
								<?php $this->load->view('submit_success') ?>
							<?php endif; ?>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_nama">Nama Lengkap</label>
								<div class="col-md-8">
									<input name="prof_nama" value="<?php echo $prof->realname; ?>" class="form-control input-md" type="text">
									<span class="help-block nobottommargin">Tuliskan gelar anda jika ada.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_birth_city">Tempat Lahir</label>
								<div class="col-md-8">
									<input name="prof_birth_city" value="<?php echo $prof->birth_city; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_birth_date">Tanggal Lahir</label>
								<div class="col-md-8">
									<input id="birthdate" name="prof_birth_date" value="<?php echo $prof->birth_date; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_gender">Jenis Kelamin</label>
								<div class="col-md-8">
									<label class="checkbox-inline" for="prof_gender-0"><input name="prof_gender" value="Laki-laki" <?php if($prof->gender == 'Laki-laki') {echo 'checked="yes"';} ?> type="checkbox">Laki-laki</label>
									<label class="checkbox-inline" for="prof_gender-1"><input name="prof_gender" value="Perempuan" <?php if($prof->gender == 'Perempuan') {echo 'checked="yes"';} ?> type="checkbox">Perempuan</label>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_home_addr">Alamat Rumah</label>
								<div class="col-md-8">
									<input name="prof_home_addr" value="<?php echo $prof->home_addr; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_home_city">Kab./Kota</label>
								<div class="col-md-8">
									<input name="prof_home_city" value="<?php echo $prof->home_city; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_home_prov">Provinsi</label>
								<div class="col-md-8">
									<select name="prof_home_prov" class="form-control">
										<option value="Belum diketahui" style="margin-bottom: 10px;">PILIH SALAH SATU</option>
										<option <?php if($prof->home_prov == 'Bangka-Belitung'){ echo 'selected = "selected"'; } ?> value="Bangka-Belitung">BANGKA-BELITUNG</option>
										<option <?php if($prof->home_prov == 'Bali'){ echo 'selected = "selected"'; } ?> value="Bali">BALI</option>
										<option <?php if($prof->home_prov == 'Banten'){ echo 'selected = "selected"'; } ?> value="Banten">BANTEN</option>
										<option <?php if($prof->home_prov == 'Bengkulu'){ echo 'selected = "selected"'; } ?> value="Bengkulu">BENGKULU</option>
										<option <?php if($prof->home_prov == 'DI Yogyakarta'){ echo 'selected = "selected"'; } ?>  value="DI Yogyakarta">DI YOGYAKARTA</option>
										<option <?php if($prof->home_prov == 'DKI Jakarta'){ echo 'selected = "selected"'; } ?> value="DKI Jakarta">DKI JAKARTA</option>
										<option <?php if($prof->home_prov == 'Gorontalo'){ echo 'selected = "selected"'; } ?> value="Gorontalo">GORONTALO</option>
										<option <?php if($prof->home_prov == 'Jambi'){ echo 'selected = "selected"'; } ?> value="Jambi">JAMBI</option>
										<option <?php if($prof->home_prov == 'Jawa Barat'){ echo 'selected = "selected"'; } ?> value="Jawa Barat">JAWA BARAT</option>
										<option <?php if($prof->home_prov == 'Jawa Tengah'){ echo 'selected = "selected"'; } ?> value="Jawa Tengah">JAWA TENGAH</option>
										<option <?php if($prof->home_prov == 'Jawa Timur'){ echo 'selected = "selected"'; } ?> value="Jawa Timur">JAWA TIMUR</option>
										<option <?php if($prof->home_prov == 'Kalimantan Barat'){ echo 'selected = "selected"'; } ?> value="Kalimantan Barat">KALIMANTAN BARAT</option>
										<option <?php if($prof->home_prov == 'Kalimantan Selatan'){ echo 'selected = "selected"'; } ?> value="Kalimantan Selatan">KALIMANTAN SELATAN</option>
										<option <?php if($prof->home_prov == 'Kalimantan Tengah'){ echo 'selected = "selected"'; } ?> value="Kalimantan Tengah">KALIMANTAN TENGAH</option>
										<option <?php if($prof->home_prov == 'Kalimantan Timur'){ echo 'selected = "selected"'; } ?> value="Kalimantan Timur">KALIMANTAN TIMUR</option>
										<option <?php if($prof->home_prov == 'Kepulauan Riau'){ echo 'selected = "selected"'; } ?> value="Kepulauan Riau">KEPULAUAN RIAU</option>
										<option <?php if($prof->home_prov == 'Lampung'){ echo 'selected = "selected"'; } ?> value="Lampung">LAMPUNG</option>
										<option <?php if($prof->home_prov == 'Maluku'){ echo 'selected = "selected"'; } ?> value="Maluku">MALUKU</option>
										<option <?php if($prof->home_prov == 'Maluku Utara'){ echo 'selected = "selected"'; } ?> value="Maluku Utara">MALUKU UTARA</option>
										<option <?php if($prof->home_prov == 'Nangroe Aceh Darussalam'){ echo 'selected = "selected"'; } ?> value="Nangroe Aceh Darussalam">NANGGROE ACEH DARUSSALAM</option>
										<option <?php if($prof->home_prov == 'Nusa Teggara Timur'){ echo 'selected = "selected"'; } ?> value="Nusa Teggara Timur">NUSA TEGGARA TIMUR</option>
										<option <?php if($prof->home_prov == 'Nusa Tenggara Barat'){ echo 'selected = "selected"'; } ?> value="Nusa Tenggara Barat">NUSA TENGGARA BARAT</option>
										<option <?php if($prof->home_prov == 'Papua'){ echo 'selected = "selected"'; } ?> value="Papua">PAPUA</option>
										<option <?php if($prof->home_prov == 'Papua Barat'){ echo 'selected = "selected"'; } ?> value="Papua Barat">PAPUA BARAT</option>
										<option <?php if($prof->home_prov == 'Riau'){ echo 'selected = "selected"'; } ?> value="Riau">RIAU</option>
										<option <?php if($prof->home_prov == 'Sulawesi Barat'){ echo 'selected = "selected"'; } ?> value="Sulawesi Barat">SULAWESI BARAT</option>
										<option <?php if($prof->home_prov == 'Sulawesi Selatan'){ echo 'selected = "selected"'; } ?> value="Sulawesi Selatan">SULAWESI SELATAN</option>
										<option <?php if($prof->home_prov == 'Sulawesi Tengah'){ echo 'selected = "selected"'; } ?> value="Sulawesi Tengah">SULAWESI TENGAH</option>
										<option <?php if($prof->home_prov == 'Sulawesi Tenggara'){ echo 'selected = "selected"'; } ?> value="Sulawesi Tenggara">SULAWESI TENGGARA</option>
										<option <?php if($prof->home_prov == 'Sulawesi Utara'){ echo 'selected = "selected"'; } ?> value="Sulawesi Utara">SULAWESI UTARA</option>
										<option <?php if($prof->home_prov == 'Sumatera Barat'){ echo 'selected = "selected"'; } ?> value="Sumatera Barat">SUMATERA BARAT</option>
										<option <?php if($prof->home_prov == 'Sumatera Selatan'){ echo 'selected = "selected"'; } ?> value="Sumatera Selatan">SUMATERA SELATAN</option>
										<option <?php if($prof->home_prov == 'Sumatera Utara'){ echo 'selected = "selected"'; } ?> value="Sumatera Utara">SUMATERA UTARA</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_phone">Nomor Ponsel</label>
								<div class="col-md-8">
									<input name="prof_phone" value="<?php echo $prof->phone; ?>" class="form-control input-md" type="text">
									<span class="help-block nobottommargin">Tuliskan hanya dengan angka, minimal 8 karakter.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_ahli">Bidang Keahlian</label>
								<div class="col-md-8">
									<input name="prof_ahli" value="<?php echo $prof->ahli_bid; ?>" class="form-control input-md" type="text">
									<span class="help-block nobottommargin">Tuliskan bidang keahlian yang anda kuasai.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_web">Website</label>
								<div class="col-md-8">
									<input name="prof_web" value="<?php echo $prof->website; ?>" class="form-control input-md" type="text">
									<span class="help-block nobottommargin">Tuliskan alamat website anda.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_fb">Facebook</label>
								<div class="col-md-8">
									<input name="prof_fb" value="<?php echo $prof->socmed_fb; ?>" class="form-control input-md" type="text">
									<span class="help-block">Tuliskan alamat Facebook anda.</span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_tw">Twitter</label>
								<div class="col-md-8">
									<input name="prof_tw" value="<?php echo $prof->socmed_tw; ?>" class="form-control input-md" type="text">
									<span class="help-block">Tuliskan alamat Twitter anda.</span>
								</div>
							</div>

							<div class="form-group nobottommargin">
								<?php echo form_hidden('member_id', $member_id);?>
								<div class="text-right col-md-12">
									<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
										<button type="submit" class="btn btn-primary nomargin" id="form-submit" name="submit" value="submit">Simpan</button>
									</div>
								</div>
							</div>
						</form>
						<!-- FORM BIODATA END -->
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

<script type="text/javascript">
	$(document).ready(function () {
		$('head').append('<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.datepick.css">');
	});
	
	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = "<?php echo base_url(); ?>assets/js/jquery.plugin.js";
	$("head").append(s);
	
	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = "<?php echo base_url(); ?>assets/js/jquery.datepick.js";
	$("head").append(s);
</script>

<script type="text/javascript">
$("input:checkbox").on('click', function() {
// in the handler, 'this' refers to the box clicked on
	var $box = $(this);
	if ($box.is(":checked")) {
		// the name of the box is retrieved using the .attr() method
		// as it is assumed and expected to be immutable
		var group = "input:checkbox[name='" + $box.attr("name") + "']";
		// the checked state of the group/box on the other hand will change
		// and the current value is retrieved using .prop() method
		$(group).prop("checked", false);
		$box.prop("checked", true);
	} else {
		$box.prop("checked", false);
	}
});

$('input[id=lefile]').change(function() {
$('#prof_avatar').val($(this).val());
});
</script>

<script>
$(function() {
	$('#birthdate').datepick({
		dateFormat: 'yyyy-mm-dd',
		minDate: new Date(1940, 1-1, 1),
		maxDate: new Date(2000, 12-1, 31),
		yearRange: '1940:2000'
	});
});
</script>
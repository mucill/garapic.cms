<?php if(count($pengabdian) > 0) : ?>
<?php foreach($pengabdian as $abdi) : ?>
<div class="panel panel-default noshadow clearfix">
	<div class="panel-heading">
		<div class="panel-title"><?php echo $abdi->abdi_title ?></div>
	</div>
	<table class="table table-bordered">
		<tr>
			<td class="text-right" width="30%">Rekan Kerja</td>
			<td><?php echo $abdi->abdi_partner ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Tahun Kegiatan</td>
			<td><?php echo $abdi->abdi_year ?></td>
		</tr>
		<tr>
			<td class="text-right" width="30%">Stakeholder</td>
			<td><?php echo $abdi->abdi_stake ?></td>
		</tr>
	</table>
	<div class="panel-body">
		<div class="text-right">
			<a href="<?php echo site_url('member/pengabdian/remove/'.$abdi->abdi_id) ?>" class="btn btn-sm btn-default remove">Hapus</a> 
			<a href="<?php echo site_url('member/pengabdian/edit/'.$abdi->abdi_id) ?>" class="btn btn-sm btn-default">Ubah</a>
		</div>		
	</div>
</div>
<?php endforeach ?>
<?php else : ?>
<h4 class="nomargin">Belum ada data</h4>
<p>Silahkan klik pada tautan dibawah ini untuk menambahkan informasi baru.</p>
<?php endif ?>
<p>
	<a href="<?php echo site_url('member/pengabdian/add') ?>" class="button button-small button-rounded button-black">Tambah</a>
</p>

<script>
	$('.remove').click(function(){
		if(confirm('Anda yakin menghapus data ini ? ')) {
			return true;
		} else {
			return false;
		}
	});
</script>
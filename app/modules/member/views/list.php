<?php if(count($member) > 0): ?>
<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin clearfix">
        <div id="posts" class="small-thumbs">
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo current_url() ?>" class="form" method="post">
                        <div class="col-md-6">
                            <input type="text" name="q" class="sm-form-control" placeholder="Keyword ..." />
                            <em>You can search uses part of name or expertise</em>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="button button-3d button-black nomargin">Search</button>
                            <?php if(isset($_POST['q'])) : ?>
                            <a href="<?php echo current_url() ?>" class="button button-3d button-black nomargin">Clear Filter</a>
                            <?php endif ?>
                        </div>
                    </form>
                <br>
                <br>
                <br>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Keahlian</th>
                                <th class="text-right">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php ((int)$this->uri->segment(2) > 0) ? $i = (int)$this->uri->segment(2) : $i = 1; foreach ($member as $list): ?>
                            <tr>
                                <td><?php echo $i++; ?> </td>
                                <td><?php echo $list->member_code ?> </td>
                                <td><?php echo highlight_phrase($list->realname, (isset($_POST['q']) ? $_POST['q'] : ''), '<strong style="color: red">', '</strong>') ?></td>
                                <td><?php echo highlight_phrase($list->ahli_bid, (isset($_POST['q']) ? $_POST['q'] : ''), '<strong style="color: red">', '</strong>') ?></td>
                                <td class="text-right"><a href="<?php echo site_url('member/detail'). '/' . $list->member_id ?> "><i class="ion-document"></i></a></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php echo $pagination ?>
        </div>
    </div>
    <?php $this->load->view('archives/sidebar') ?>
</section>
<?php else: ?>
<?php //show_404() ?>
<?php endif ?>
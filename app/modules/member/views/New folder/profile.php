<section class="container clearfix">
	<div class="row">
		<div class="panel panel-default noshadow">
			
			<div class="panel-body norightpadding noleftpadding">
				<div class="tab-content">
					<div class="col-md-4 panel nobottommargin">
						<!-- PHOTO PROFILE BEGIN -->
						<div class="panel panel-default noshadow">
							<div class="panel-heading">
								<h4 class="nobottommargin">Photo Profil</h4>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div class="img-profile">
											<img src="<?php if($prof->avatar == NULL){ echo base_url('uploads/person').'/person.png'; } else { echo base_url('uploads/person').'/'.$prof->avatar; } ?>" />
											<div class="portfolio-overlay">
													<a href="http://vimeo.com/89396394" class="center-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="panel-footer clearfix">
								<?php echo form_open(current_url(), 'enctype="multipart/form-data" class="nobottommargin" name="avatar-form"') ?>
									<div class="form-group nobottommargin clearfix">
										<div class="col-sm-12 input-group">
											<?php echo form_hidden('member_id', $member_id);?>
											<label for="prof_avatar" class="control-label" style="display:none;">Avatar</label>
											<input id="prof_avatar" name="prof_avatar" type="file" style="display: none;">
											<input id="prof_avatar1" name="prof_avatar1" class="form-control input-md" type="text"><hr />
										</div>
										<div class="col-sm-6 text-left">
											<div class="input-group-btn"><a class="btn btn-warning" onclick="$('input[id=prof_avatar]').click();">Browse</a></div>
										</div>
										<div class="col-sm-6 text-right">
											<button class="btn btn-primary" name="submit" value="submit" type="submit">Upload</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- PHOTO PROFILE END -->
					</div>
					<div class="col-md-8 panel nobottommargin">
						<?php if($custom_error == '') : ?>
						<?php else: ?>
							<?php $this->load->view('submit_error') ?>
						<?php endif; ?>
						<?php if($success == '') : ?>
						<?php else: ?>
							<?php $this->load->view('submit_success') ?>
						<?php endif; ?>
						<div class="panel panel-default noshadow">
							<div class="panel-heading">
								<h4 class="nobottommargin">Data Pribadi</h4>
							</div>
							<div class="panel-body">
								<ul class="list-group nobottommargin nopadding">
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Nama Lengkap</div>
										<div class="col-md-8 nomargin"><?php echo $prof->realname; ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Email</div>
										<div class="col-md-8 nomargin"><?php echo $prof->email; ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Tempat, tanggal lahir</div>
										<div class="col-md-8 nomargin"><?php if($prof->birth_city == NULL){ echo '-'; } else { echo $prof->birth_city .', '. $prof->birth_date; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Jenis Kelamin</div>
										<div class="col-md-8 nomargin"><?php if($prof->gender == NULL){ echo '-'; } else { echo $prof->gender; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Alamat Rumah</div>
										<div class="col-md-8 nomargin"><?php if($prof->home_addr == NULL){ echo '-'; } else { echo $prof->home_addr .', '. $prof->home_city .', '. $prof->home_prov; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Nomor Ponsel</div>
										<div class="col-md-8 nomargin"><?php if($prof->phone == NULL){ echo '-'; } else { echo $prof->phone; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Bidang Keahlian</div>
										<div class="col-md-8 nomargin"><?php if($prof->ahli_bid == NULL){ echo '-'; } else { echo $prof->ahli_bid; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Website</div>
										<div class="col-md-8 nomargin"><?php if($prof->website == NULL){ echo '-'; } else { echo '<a href="'.$prof->website.'" target="_blank" >'. $prof->website .'</a>'; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Facebook</div>
										<div class="col-md-8 nomargin"><?php if($prof->socmed_fb == NULL){ echo '-'; } else { echo '<a href="'.$prof->socmed_fb.'" target="_blank" >'. $prof->socmed_fb .'</a>'; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Twitter</div>
										<div class="col-md-8 nomargin"><?php if($prof->socmed_tw == NULL){ echo '-'; } else { echo '<a href="'.$prof->socmed_tw.'" target="_blank" >'. $prof->socmed_tw .'</a>'; } ?></div>
									</li>
								</ul>
							</div>
							<div class="panel-footer text-right">
								<a href="<?php echo base_url(); ?>dashboard/biodata"><button class="btn btn-warning">Ubah Data</button></a>
								<a href="<?php echo base_url(); ?>dashboard/biodata/<?php echo $member_id; ?>"><button class="btn btn-warning">Ubah Data</button></a>
							</div>
						</div>
						
						<div class="panel panel-default noshadow">
							<div class="panel-heading">
								<h4 class="nobottommargin">Data Pendidikan</h4>
							</div>
							<div class="panel-body">
								<ul class="list-group nobottommargin nopadding">
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Tingkat Pendidikan</div>
										<div class="col-md-8 nomargin"><?php if($prof->sarjana == NULL){ echo '-'; } else { echo $prof->sarjana; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Jurusan</div>
										<div class="col-md-8 nomargin"><?php if($prof->jurusan == NULL){ echo '-'; } else { echo $prof->jurusan; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Kampus Almamater</div>
										<div class="col-md-8 nomargin"><?php if($prof->kampus == NULL){ echo '-'; } else { echo $prof->kampus; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Tahun Lulus</div>
										<div class="col-md-8 nomargin"><?php if($prof->lulus == NULL){ echo '-'; } else { echo $prof->lulus; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Judul Skripsi/Tesis/Disertasi</div>
										<div class="col-md-8 nomargin"><?php if($prof->skripsi == NULL){ echo '-'; } else { echo $prof->skripsi; } ?></div>
									</li>
								</ul>
							</div>
							<div class="panel-footer text-right">
								<div class="btn-group">
									<a href="<?php echo base_url(); ?>dashboard/pendidikan/add"><button class="btn btn-success">Tambah Data</button></a>
									<a href="<?php echo base_url(); ?>dashboard/pendidikan"><button class="btn btn-warning">Ubah Data</button></a>
								</div>
							</div>
						</div>
						
						<div class="panel panel-default noshadow">
							<div class="panel-heading">
								<h4 class="nobottommargin">Data Instansi</h4>
							</div>
							<div class="panel-body">
								<ul class="list-group nobottommargin nopadding">
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Nama Instansi</div>
										<div class="col-md-8 nomargin"><?php if($prof->office_name == NULL){ echo '-'; } else { echo $prof->office_name; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Alamat Instansi</div>
										<div class="col-md-8 nomargin"><?php if($prof->office_addr == NULL){ echo '-'; } else { echo $prof->office_addr .', '. $prof->office_city .', '. $prof->office_prov; } ?></div>
									</li>
								</ul>
							</div>
							<div class="panel-footer text-right">
								<a href="<?php echo base_url(); ?>dashboard/instansi"><button class="btn btn-warning">Ubah Data</button></a>
							</div>
						</div>
						
						<div class="panel panel-default noshadow">
							<div class="panel-heading">
								<h4 class="nobottommargin">Data Jabatan</h4>
							</div>
							<div class="panel-body">
<?php foreach ($getJab as $jab): ?>
								<ul class="list-group nobottommargin nopadding">
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="media-body">
											<div class="col-sm-4 nomargin listProf">Jenis Jabatan</div>
											<div class="col-sm-8 nomargin"><?php if($jab->jbt_type == NULL){ echo '-'; } else { echo $jab->jbt_type; } ?></div>
											<div class="col-sm-4 nomargin listProf">Institusi</div>
											<div class="col-sm-8 nomargin"><?php if($jab->jbt_office == NULL){ echo '-'; } else { echo $jab->jbt_office; } ?></div>
											<div class="col-sm-4 nomargin listProf">Periode Jabatan</div>
											<div class="col-sm-8 nomargin"><?php if($jab->jbt_periode == NULL){ echo '-'; } else { echo $jab->jbt_periode; } ?></div>
										</div>
										<div class="media-right">
											<a href="<?php base_url(); ?>jabatan/update/<?php echo $jab->jabatan_id; ?>">
												<button class="btn btn-warning" title="Ubah Data"><i class="icon icon-edit" style="font-size: 16px;"></i></button>
											</a>
										</div>
									</li>
								</ul>
<?php endforeach; ?>
							</div>
							<div class="panel-footer text-right">
								<div class="btn-group">
									<a href="<?php base_url(); ?>jabatan/add"><button class="btn btn-success">Tambah Data</button></a>
								</div>
							</div>
						</div>
						
						<div class="panel panel-default noshadow">
							<div class="panel-heading">
								<h4 class="nobottommargin">Data Publikasi</h4>
							</div>
							<div class="panel-body">
								<ul class="list-group nobottommargin nopadding">
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Judul Publikasi</div>
										<div class="col-md-8 nomargin"><?php if($prof->pub_title == NULL){ echo '-'; } else { echo $prof->pub_title; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Penulis Lain</div>
										<div class="col-md-8 nomargin"><?php if($prof->pub_partner == NULL){ echo '-'; } else { echo $prof->pub_partner; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Jenis Publikasi</div>
										<div class="col-md-8 nomargin"><?php if($prof->pub_type == NULL){ echo '-'; } else { echo $prof->pub_type; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">ISBN/ISSN</div>
										<div class="col-md-8 nomargin"><?php if($prof->pub_isbn == NULL){ echo '-'; } else { echo $prof->pub_isbn; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Tahun Terbit</div>
										<div class="col-md-8 nomargin"><?php if($prof->pub_year == NULL){ echo '-'; } else { echo $prof->pub_year; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Abstract</div>
										<div class="col-md-8 nomargin"><?php if($prof->pub_abstract == NULL){ echo '-'; } else { echo $prof->pub_abstract; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Attachment</div>
										<div class="col-md-8 nomargin"><?php if($prof->pub_file == NULL){ echo '-'; } else { echo $prof->pub_file; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Link Publikasi</div>
										<div class="col-md-8 nomargin"><?php if($prof->pub_link == NULL){ echo '-'; } else { echo $prof->pub_link; } ?></div>
									</li>
								</ul>
							</div>
							<div class="panel-footer text-right">
								<div class="btn-group">
									<a href="<?php echo base_url(); ?>dashboard/publikasi/add"><button class="btn btn-success">Tambah Data</button></a>
									<a href="<?php echo base_url(); ?>dashboard/publikasi"><button class="btn btn-warning">Ubah Data</button></a>
								</div>
							</div>
						</div>
						
						<div class="panel panel-default noshadow">
							<div class="panel-heading">
								<h4 class="nobottommargin">Data Penelitian</h4>
							</div>
							<div class="panel-body">
								<ul class="list-group nobottommargin nopadding">
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Judul Penelitian</div>
										<div class="col-md-8 nomargin"><?php if($prof->riset_title == NULL){ echo '-'; } else { echo $prof->riset_title; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Mitra Penelitian</div>
										<div class="col-md-8 nomargin"><?php if($prof->riset_partner == NULL){ echo '-'; } else { echo $prof->riset_partner; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Tahun Penelitian</div>
										<div class="col-md-8 nomargin"><?php if($prof->riset_year == NULL){ echo '-'; } else { echo $prof->riset_year; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Sumber Dana Penelitian</div>
										<div class="col-md-8 nomargin"><?php if($prof->riset_fund == NULL){ echo '-'; } else { echo $prof->riset_fund; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Stakeholder</div>
										<div class="col-md-8 nomargin"><?php if($prof->riset_stake == NULL){ echo '-'; } else { echo $prof->riset_stake; } ?></div>
									</li>
								</ul>
							</div>
							<div class="panel-footer text-right">
								<div class="btn-group">
									<a href="<?php echo base_url(); ?>dashboard/penelitian/add"><button class="btn btn-success">Tambah Data</button></a>
									<a href="<?php echo base_url(); ?>dashboard/penelitian"><button class="btn btn-warning">Ubah Data</button></a>
								</div>
							</div>
						</div>
						
						<div class="panel panel-default noshadow nobottommargin">
							<div class="panel-heading">
								<h4 class="nobottommargin">Data Pengabdian</h4>
							</div>
							<div class="panel-body">
								<ul class="list-group nobottommargin nopadding">
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Judul Pengabdian</div>
										<div class="col-md-8 nomargin"><?php if($prof->abdi_title == NULL){ echo '-'; } else { echo $prof->abdi_title; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Mitra Pengabdian</div>
										<div class="col-md-8 nomargin"><?php if($prof->abdi_partner == NULL){ echo '-'; } else { echo $prof->abdi_partner; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Tahun Pengabdian</div>
										<div class="col-md-8 nomargin"><?php if($prof->abdi_year == NULL){ echo '-'; } else { echo $prof->abdi_year; } ?></div>
									</li>
									<li class="list-group-item noleftpadding  norightpadding  clearfix">
										<div class="col-md-4 nomargin listProf">Stakeholder</div>
										<div class="col-md-8 nomargin"><?php if($prof->abdi_stake == NULL){ echo '-'; } else { echo $prof->abdi_stake; } ?></div>
									</li>
								</ul>
							</div>
							<div class="panel-footer text-right">
								<div class="btn-group">
									<a href="<?php echo base_url(); ?>dashboard/pengabdian/add"><button class="btn btn-success">Tambah Data</button></a>
									<a href="<?php echo base_url(); ?>dashboard/pengabdian"><button class="btn btn-warning">Ubah Data</button></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>

<script type="text/javascript">
$('input[id=prof_avatar]').change(function() {
$('#prof_avatar1').val($(this).val());
});
</script>
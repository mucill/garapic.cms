<?php if(count($schedule) > 0): ?>
<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin clearfix">
        <div id="posts" class="small-thumbs">
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo current_url() ?>" class="form" method="post">
                        <div class="col-md-6">
                            <input type="text" name="q" class="sm-form-control" placeholder="Keyword ..." />
                            <em>You can search uses part of event title</em>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="button button-3d button-black nomargin">Search</button>
                            <?php if(isset($_POST['q'])) : ?>
                            <a href="<?php echo current_url() ?>" class="button button-3d button-black nomargin">Clear Filter</a>
                            <?php endif ?>
                        </div>
                    </form>
                <br>
                <br>
                <br>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><?php echo __('event', false) ?></th>
                                <th><?php echo __('location', false) ?> </th>
                                <th><?php echo __('date', false) ?></th>
                                <th class="text-right"><?php echo __('detail', false) ?> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            foreach ($schedule as $sch) : ?>
                            <tr>
                                <td>
                                    <a href="<?php echo site_url('schedule/read/' . $sch->id) ?>"><?php echo $sch->title ?></a>
                                </td>
                                <td><?php echo $sch->place ?> </td>
                                <td>
                                    <?php echo mdate('%d %M %Y', strtotime($sch->start_date)) ?>
                                </td>
                                <td class="text-right">
                                    <a href="<?php echo site_url('schedule/read/' . $sch->id) ?>"><i class="ion-android-calendar"></i></a>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php echo $pagination ?>
        </div>
    </div>
    <?php $this->load->view('archives/sidebar') ?>
</section>
<?php else: ?>
<?php //show_404() ?>
<?php endif ?>
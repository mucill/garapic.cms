<section class="container topmargin">
    <div class="col-md-3">
        <div class="list-group">
        <a href="<?php echo site_url('message/create') ?>" class="list-group-item">Create a Message</a>
        </div>
        <div class="list-group">            
            <a href="<?php echo site_url('message/inbox') ?>" class="list-group-item">Inbox <span class="badge"><?php echo ($unread > 0 ) ? $unread : '' ?></span></a>
            <a href="<?php echo site_url('message/outbox') ?>" class="list-group-item">Outbox</a>
        </div>                    
    </div>
    <div class="col-md-9">
        <?php echo $message_content ?>
    </div>
</section>


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

    private $data            = array();
    private $url            = 'message';
    private $perpage        = 10;

    public function __construct()
    {
        parent::__construct();

        // check if user logged or not
        if(empty($this->session->userdata('member_id'))) {
            redirect(site_url(),'refresh');
        }

        $this->data['title'] = __('message', false);

        // set default value
        $unread   = 0;
        $this->data['lists']    = array();
        $current_user           = $this->session->userdata('member_id');

        // show unread counter
        $this->db
        ->where('to', $current_user)
        ->where('status', 0);
        $query = $this->db->get('cms_message');
        if($query->num_rows() > 0) {
            $unread = $query->num_rows();            
        }

        $this->data['unread']           = $unread;
     
        $this->output->enable_profiler(false);
    }

    public function index()
    {   
        $this->inbox();      
    }

    public function inbox()
    {   
        // title
        $this->data['title']    = 'Inbox Message';

        // set default value
        $this->data['lists']    = array();
        $current_user           = $this->session->userdata('member_id');

        // get data
        $this->db
        ->select('a.id, a.subject, a.status, b.realname name')
        ->from('cms_message a')
        ->join('cms_member b', 'b.member_id = a.from', 'left')
        ->where('to', $current_user);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            $this->data['lists'] = $query->result();
        } 
        $this->data['message_content']  = $this->load->view('list', $this->data, true);
        $this->data['content']          = $this->load->view('message', $this->data, true);
        $this->load->view('master/content', $this->data);      
    }

    public function outbox()
    { 
        // title
        $this->data['title']    = 'Outbox Message';

        // set default value
        $this->data['lists']    = array();
        $current_user           = $this->session->userdata('member_id');

        // get data
        $this->db
        ->select('a.id, a.subject, a.status, b.realname name')
        ->from('cms_message a')
        ->join('cms_member b', 'b.member_id = a.to', 'left')
        ->where('from', $current_user);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            $this->data['lists'] = $query->result();
        }
        $this->data['message_content'] = $this->load->view('list', $this->data, true);
        $this->data['content'] = $this->load->view('message', $this->data, true);
        $this->load->view('master/content', $this->data);      
    }

    public function create()
    {
        // title
        $this->data['title']    = 'Create Message';

        // get member list for sender purpose
        $members = array();
        $this->db->where('member_id <>', $this->session->userdata('member_id'));
        $this->db->select('member_id, realname');
        $query  = $this->db->get('cms_member');
        if($query->num_rows() > 0 ) {
            foreach ($query->result() as $member) {
                $members[$member->member_id] = $member->realname;
            }
        }
        $this->data['members']              = $members;

        // current logged member
        $this->data['from_id']              = $this->session->userdata('member_id');
        $this->data['from']                 = $this->session->userdata('realname');

        $this->form_validation->set_rules('to[]', 'to', 'required');
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('message', 'message', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->data['message_content']  = $this->load->view('create', $this->data, true);
            $this->data['content']          = $this->load->view('message', $this->data, true);
        } else {
            foreach($this->input->post('to') as $to) {
                $record = array(
                    'to'        => $to,
                    'from'      => $this->input->post('from'),
                    'subject'   => $this->input->post('subject'),
                    'message'   => $this->input->post('message')
                );
                $this->db->insert('cms_message', $record);
            }
            $this->data['message']          = 'Message has been send succesfully.';
            $this->data['button_link']      = site_url('message');
            $this->data['content']          = $this->load->view('master/redirect', $this->data, true);            
        }

        $this->load->view('master/content', $this->data);
    }

    public function read($id = null)
    {
        // update status
        $this->db->where('id', $id);
        $this->db->update('cms_message', array('status' => 1));

        // show unread counter
        $current_user           = $this->session->userdata('member_id');
        $unread = 0;
        $this->db
        ->where('to', $current_user)
        ->where('status', 0);
        $query = $this->db->get('cms_message');
        if($query->num_rows() > 0) {
            $unread = $query->num_rows();            
        }

        $this->data['unread']           = $unread;

        // is message available
        $this->db->where('id', $id);
        $query = $this->db->get('cms_message');
        if($query->num_rows() <= 0) {
            $this->data['message'] = 'Data not found.';
            $this->data['content'] = $this->load->view('master/blank', $this->data, true);
        } else {
            $destination = $query->row();

            // if session member_id equal to from field
            // show the current read action is from outbox
            // instead, its from inbox
            $this->db
            ->select('a.id, a.to, a.from, b.realname, a.subject, a.message')
            ->from('cms_message a')
            ->where('a.id', $id);

            if($destination->from == $this->session->userdata('member_id')) {
                $this->db->join('cms_member b', 'b.member_id = a.to', 'left');
        
                // title
                $this->data['title']    = 'Outbox Message';
            } else {
                $this->db->join('cms_member b', 'b.member_id = a.from', 'left');                

                // title
                $this->data['title']    = 'Inbox Message';
            }

            // get member list for sender purpose
            $query  = $this->db->get('cms_message');
            $message = $query->row();

            // current logged member
            if($message->from == $this->session->userdata('member_id')) {
                $this->data['to']       = $this->session->userdata('realname');
                $this->data['from']     = $message->realname;
            } else {
                $this->data['to']       = $message->realname;
                $this->data['from']     = $this->session->userdata('realname');    
            }
            $this->data['subject']  = $message->subject;
            $this->data['message']  = $message->message;

            $this->data['message_content']  = $this->load->view('read', $this->data, true);
            $this->data['content']          = $this->load->view('message', $this->data, true);
        }

        $this->load->view('master/content', $this->data);
    }

    public function reply($id = null)
    {
        // title
        $this->data['title']    = 'Reply Message';

        // current logged member
        $this->data['from_id']              = $this->session->userdata('member_id');
        $this->data['from']                 = $this->session->userdata('realname');

        // read current message
        $this->db
        ->select('a.from from, b.realname name, a.message message, a.subject subject')
        ->from('cms_message a')
        ->join('cms_member b', 'b.member_id = a.from','left')
        ->where('a.id', $id);
        $query = $this->db->get();
        $reply = $query->row();

        $this->data['to_id']                = $reply->from;
        $this->data['to']                   = $reply->name;
        $this->data['subject']              = 'Re: '.$reply->subject;
        $this->data['message']              = $reply->message.'&#13;&#10;========================&#13;&#10;';

        $this->form_validation->set_rules('to', 'to', 'required');
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('message', 'message', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->data['message_content']  = $this->load->view('reply', $this->data, true);
            $this->data['content']          = $this->load->view('message', $this->data, true);
        } else {
            $record = array(
                'to'        => $this->input->post('to'),
                'from'      => $this->input->post('from'),
                'subject'   => $this->input->post('subject'),
                'message'   => $this->input->post('message')
            );
            $this->db->insert('cms_message', $record);
            $id_insert = $this->db->insert_id();

            // set reply field for current data
            $this->db->where('id', $id);
            $this->db->update('cms_message', array('reply' => $id_insert));

            $this->data['message']          = 'Message has been send succesfully.';
            $this->data['button_link']      = site_url('message');
            $this->data['content']          = $this->load->view('master/redirect', $this->data, true);            
        }

        $this->load->view('master/content', $this->data);
    }

}

/* End of file message.php */
/* Location: .//D/Webs/mgi/app/modules/message/controllers/message.php */
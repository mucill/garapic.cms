<section id="news-title" class="section notopmargin noborder nobottommargin" >
    <div class="container clearfix">
        <div class="heading-block nobottommargin noborder core-heading-block">
        	<a href="<?php echo site_url('/member/register') ?>" class="button button-border button-rounded fright nomargin"><?php __('registration') ?></a>
            <h4><?php __('registration') ?></h4>
            <span><?php __('registration_info') ?></span>
        </div>
    </div>
</section>

<?php if(count($files) > 0) : ?>
<table class="table">
    <?php foreach($files as $file) : ?>
        <tr>
            <td width="48px">
                <div style="width:64px; overflow: hidden;">
                <img class="img-thumbnail" src="<?php echo site_url('files/media') . '/' . $file->filecode ?>" alt="<?php echo $file->filename ?>" width="64px" />
                </div>                                     
            </td>
            <td>
                <a href="#" data-id="<?php echo $file->id ?>" data-name="<?php echo $file->filename ?>">
                <?php echo ($file->alt != '') ? $file->alt : $file->filename ?>
                <span class="mailbox-attachment-size"><?php echo $file->size ?> KB</span>
                </a>
            </td>
            <td>
                <?php
                if($this->menu->crud_access('delete')) {
                    echo '<a href="'.site_url('files/delete/'.$file->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a> ';
                }
                if($this->menu->crud_access('update')) {
                    echo '<a href="'.site_url('files/update/'.$file->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a> ';
                }
                ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>
<?php else :?>
<div class="text-center">
    No Media Available. <a href="<?php echo base_url('files/add') ?>">Upload New Media</a>
</div>
<?php endif ?>
<script type="text/javascript">
    $('#media table a').each(function(index){
        $(this).click(function(){
            id      = $(this).data('id');
            name    = $(this).data('name');
            tpl     = '<a class="list-group-item">' + name + '<span class="pull-right"><i class="ion ion-close" style="cursor: pointer;" onclick="do_remove(this)"></i></span><input type="hidden" name="attach_list[]" value="' + id + '"></a>';

            $('#attach_list').prepend(tpl);
            $('#attach_btn').focus();
        });
    });

    $('.delete').click(function(){
        if(confirm('Are you sure delete this record ? ')) {
            return true;
        } else {
            return false;
        }
    });
</script>
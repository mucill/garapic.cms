<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends MX_Controller { 

    private $table_name     = 'media';
    private $url            = 'files';
    private $perpage        = 6;
    private $data           = array();
    private $update         = '';
    private $delete         = '';
    private $upload_path    = MEDIA .'/';
    private $mime_allowed   = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|dot|pdf';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // load library
        $this->load->library('upload');
        $this->load->helper('text');

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Media Files';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // folders
        $this->db
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'join')
        ->where('a.type', 'folder')
        ->group_by('a.id');
        $query = $this->db->get();
        $this->data['folder']  = $query->result();

        // pagination
        $this->db
        ->select('a.id')
        ->from($this->table_name.' a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->group_by('a.id');
        $query = $this->db->get();
        $count                      = $query->num_rows();

        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url.'/ajaxindex'), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // all data
        $this->db
        ->from($this->table_name . ' a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->order_by('a.updated_at','desc')
        ->group_by('a.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            $list                   = $query->result();
            $this->data['files']    = $list;
        } else {
            $this->data['files'] = array();
        }

        $this->data['js_files'] = array(
            base_url() . PLUGINS. 'twbs-pagination/jquery.twbsPagination.min.js'
        );

        $this->data['scripts'] = '
            $(".pagination").twbsPagination({
                totalPages: '.ceil($count / $this->perpage).',
                onPageClick: function (event, page) {
                    $.post("'.site_url($this->url.'/ajaxindex').'/" + page, function(data) {
                        $("#files").html(data);
                    })
                }
            });';

        $this->data['content']      = $this->load->view('files/list', $this->data, TRUE);

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    public function ajaxindex( $offset = 0, $filter = '' )
    {
        if($offset > 1 ) {
            $off = ($offset * $this->perpage ) / $offset;
        } else {
            $off = 0;
        }

        // all data
        $this->db
        ->from($this->table_name)
        ->order_by('updated_at','desc')
        ->group_by('id')
        ->limit($this->perpage, $off);
        $query = $this->db->get();

        // if not empty
        // return data
        if($query->num_rows() > 0) {
            $list                   = $query->result();
            $this->data['files']    = $list;
        } else {
            $this->data['files'] = array();
        }

        $this->load->view('files/list_ajax', $this->data);
    }


    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get folder
        $this->db
        ->select('id, name')
        ->from('category')
        ->where('type', 'folder')
        ->group_by('id');
        $query  = $this->db->get();
        $folder = $query->result();
        foreach($folder as $fold) {
            $this->data['folders'][$fold->id] = $fold->name;
        }

        // validation rules
        $this->form_validation->set_rules("alt", "Name", 'required');
        $this->form_validation->set_rules("folder", "Folder", 'required');
        // $this->form_validation->set_rules("userfile", "File", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('files/add', $this->data, true);
        } else {

            // if not break the rules 
            $file_name     = '';
            if (!empty($_FILES['userfile']['name'])) {
                $path_parts                     = pathinfo($_FILES["userfile"]["name"]);
                $extension                      = $path_parts['extension'];
                $config['upload_path']          = $this->upload_path;
                $config['allowed_types']        = $this->mime_allowed;
                $config['file_name']            = str_pad($this->input->post('folder'), 3, "0", STR_PAD_LEFT).'_'.$_FILES['userfile']['name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $metafile                       = $this->upload->data();
                $file_name                      = $_FILES["userfile"]["name"];
                $file_type                      = $metafile['file_type'];
                $file_size                      = $metafile['file_size'];
            }

            // firstly, save to media
            $record = array(
                'category_id'   => $this->input->post('folder') ,
                'filename'      => str_pad($this->input->post('folder'), 3, "0", STR_PAD_LEFT).'_'.$file_name,
                'alias'      => md5($file_name),
                'type'          => $file_type,
                'size'          => $file_size,
                'alt'           => $this->input->post('alt'),
                'desc'          => $this->input->post('desc'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();

            // check for status
            $this->data['message']  = 'Your record has been saved.';
            $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    // Add a new item
    public function update( $id = NULL )
    {
        // security check
        $this->menu->check_access('update');

        // get folder
        $this->db
        ->select('a.id, b.name')
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'left')
        ->where('type', 'folder')
        ->group_by('id');
        $query  = $this->db->get();
        $folder = $query->result();
        foreach($folder as $fold) {
            $this->data['folders'][$fold->id] = $fold->name;
        }

        // get current data
        $this->db
        ->from($this->table_name. ' a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->join('category_lang c', 'c.id = a.category_id', 'left')
        ->where('a.id', $id);
        $query = $this->db->get();

        // if no data
        if($query->num_rows() <= 0 ) {
            return show_error('Your data may has been removed or changes by someone.', '' , 'Data Not Found');
            exit;
        }

        // return data
        $this->data['form']     = $query->row();

        // validation rules
        $this->form_validation->set_rules("alt", "Name", 'required');
        $this->form_validation->set_rules("folder", "Folder", 'required');
        // $this->form_validation->set_rules("userfile", "File", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('files/edit', $this->data, true);
        } else {

            // if not break the rules 
            $file_name                          = '';
            if (!empty($_FILES['userfile']['name'])) {
                // remove current file
                if($this->data['form']->filename != '') {
                    @unlink($this->upload_path . $this->data['form']->filename);                
                }

                // upload new file
                $path_parts                     = pathinfo($_FILES["userfile"]["name"]);
                $extension                      = $path_parts['extension'];
                $config['upload_path']          = $this->upload_path;
                $config['allowed_types']        = $this->mime_allowed;
                $config['file_name']            = str_pad($this->input->post('folder'), 3, "0", STR_PAD_LEFT).'_'.$_FILES['userfile']['name'];
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $metafile                       = $this->upload->data();
                $file_name                      = $config['file_name'];
                $file_type                      = $metafile['file_type'];
                $file_size                      = $metafile['file_size'];
            }

            // firstly, save to media
            $record = array(
                'category_id'   => $this->input->post('folder') ,
                'filename'      => str_pad($this->input->post('folder'), 3, "0", STR_PAD_LEFT).'_'.$file_name,
                'alias'         => md5($file_name),
                'type'          => $file_type,
                'size'          => $file_size,
                'alt'           => $this->input->post('alt'),
                'desc'          => $this->input->post('desc'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->where('id', $id);
            $this->db->update($this->table_name, $record);
            // $id = $this->db->insert_id();

            // check for status
            $this->data['message']  = 'Your record has been saved.';
            $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Delete one item
    public function delete( $id = NULL )
    {

        // security check
        $this->menu->check_access('delete');

        // get current filename
        $this->db
        ->select('filename')
        ->where('id', $id);
        $query  = $this->db->get($this->table_name);
        $file   = $query->row(); 

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            // let's remove it
            $message = 'Your record has been removed.';
            @unlink($this->upload_path . $file->filename);
        }
        $this->data['button_link']      = site_url($this->url);
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function getParent($parent_id)
    {   
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('category')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();                
                $output = $parent->category;            
            }
        }
        return $output;
    }

    public function folder( $filter = '', $offset = 0 )
    {        
        // folders
        $this->db
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'join')
        ->where('a.type', 'folder')
        ->group_by('a.id');
        $query = $this->db->get();
        $this->data['folder']  = $query->result();

        // pagination 
        $this->db
        ->select('id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url('/files/folder').'/'.$filter, $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->from($this->table_name. ' a')
        ->join('category_lang b', 'b.id = a.category_id', 'left')
        ->join('media_lang c', 'c.id = a.id', 'left')
        ->where('b.slug', $filter)
        ->order_by('a.updated_at')
        ->group_by('a.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // return data
            $list                   = $query->result();
            $this->data['files']    = $list;

        } else {
            $this->data['files'] = array();
        }

        $this->data['content']      = $this->load->view('files/list', $this->data, TRUE);

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    public function media($alias)
    {
        $this->db
        ->where('alias', $alias);
        $query = $this->db->get('media');
        if($query->num_rows() > 0 ) {
            $read   = $query->row();
            $mime   = $read->type;
            if((substr($mime, -3) == 'peg') || (substr($mime, -3) == 'jpg') || (substr($mime, -3) == 'png') || (substr($mime, -3) == 'gif')) {
                $path   = base_url() . $this->upload_path . $read->filename;            
            } else {
                $path   = base_url() . REPOSITORY . 'blank.jpg';            
                $mime   = 'image/jpg';                
            }
        } else {
            $path   = base_url() . REPOSITORY . 'blank.jpg';            
            $mime   = 'image/jpg';
        }
        // debug($path);
        header("Content-Type: ".$mime);
        readfile($path);
        exit;
    }


}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */

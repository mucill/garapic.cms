<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<?php echo form_open(current_url(), 'class="form-horizontal" role="form"') ?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
            <div class="form-group">
                <label for="" class="col-sm-4">Name</label>
                <div class="col-sm-8">
                <?php echo form_input('name', set_value('name'), 'class="form-control"') ?>
                </div>
            </div>                              
            <div class="form-group">
                <label for="" class="col-sm-4">Parent</label>
                <div class="col-sm-8">
                <?php 
                echo form_dropdown('parent', $dropdown_opt, 0, array('class'=>'form-control'));    
                ?>
                </div>
            </div>                              
            <div class="form-group">
                <label for="" class="col-sm-4">Show / Hide On Navigation</label>
                <div class="col-sm-8">
                <?php echo form_dropdown('show', array('1' => 'Show', '0' => 'Hide'), 1, 'class="form-control"'); ?>
                </div>
            </div>                              
            <div class="form-group">
                <label for="" class="col-sm-4">Classname</label>
                <div class="col-sm-8">
                <?php echo form_input('path', set_value('path'), 'class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-4">Icon</label>
                <div class="col-sm-8">
                <?php echo form_input('icon', set_value('icon'), 'class="form-control"') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-4">Available Method</label>
                <div class="col-sm-8">
                    <div class="checkbox">
                    <label for="write">
                    <?php echo form_checkbox('has_write', '1', set_checkbox('has_write','1'), 'id="write" style="margin-top:1px;"'); ?> Write
                    </label>
                    </div>
                    <div class="checkbox">
                    <label for="update">
                    <?php echo form_checkbox('has_update', '1', set_checkbox('has_update','1'), 'id="update" style="margin-top:1px;"'); ?> Update
                    </label>
                    </div>
                    <div class="checkbox">
                    <label for="delete">
                    <?php echo form_checkbox('has_delete', '1', set_checkbox('has_delete','1'), 'id="delete" style="margin-top:1px;"'); ?> Delete
                    </label>
                    </div>
                </div>
            </div>                              
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('packages') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>
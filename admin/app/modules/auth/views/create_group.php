<?php echo form_open('admin/auth/create_group', 'class="form-horizontal" role="form"');?>
    <?php if($message) : ?>
    <div class="alert alert-danger"><?php echo $message ?></div>
    <?php endif ?>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Please complete the form below.</h3>
        </div>
        <div class="box-body tm-padding">            
            <div class="form-group">
                <label for="" class="col-sm-4">Group Name *</label>
                <div class="col-sm-8">
                <?php echo form_input($group_name,'','class="form-control"');?>
                </div>
            </div>
                 
            <div class="form-group">
                <label for="" class="col-sm-4">Description</label>
                <div class="col-sm-8">
                <?php echo form_input($description,'','class="form-control"');?>
                </div>
            </div>                                 
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('groups') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
    </div>

<?php echo form_close();?>

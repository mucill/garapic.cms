<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Upload file.</h3>
    </div>
    <ul class="nav nav-tabs" role="tablist" style="padding-left: 10px;">
        <?php foreach($language as $lang) : ?>
        <li role="presentation" <?php echo ($lang->is_default == 1) ? 'class="active"' : '' ?>><a href="#lang<?php echo $lang->shortname ?>" aria-controls="<?php echo $lang->shortname ?>" role="tab" data-toggle="tab" style="padding: 25px 15px;"><?php echo $lang->name ?></a></li>
        <?php endforeach ?>
    </ul>
    <div class="box-body tab-content"> 
        <?php foreach($language as $lang) : ?>
            <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : '' ?>" id="lang<?php echo $lang->shortname ?>">
                <div class="form-group">
                    <label for="" class="col-sm-4">Alternative name in <?php echo $lang->name ?></label>
                    <div class="col-sm-8">
                    <input type="text" name="alt[<?php echo $lang->shortname ?>]" value="<?php echo set_value("alt[".$lang->shortname."]", $form[$lang->shortname]->alt) ?>" class="form-control">
                    </div>
                </div>              
                <div class="form-group">
                    <label for="" class="col-sm-4">Description in <?php echo $lang->name ?></label>
                    <div class="col-sm-8">
                    <textarea name="desc[<?php echo $lang->shortname ?>]" class="form-control"><?php echo set_value("desc[".$lang->shortname."]", $form[$lang->shortname]->desc) ?></textarea>
                    </div>
                </div>              
            </div>
        <?php endforeach ?>
        <div class="form-group">
            <label for="" class="col-sm-4">Folder</label>
            <div class="col-sm-8">
            <?php if(isset($folders) > 0 ) : ?>
            <?php echo form_dropdown('folder', $folders, '', 'class="form-control"') ?>
            <?php else : ?>
            <p style="padding-top: 10px;"><a href="<?php echo site_url('folders/add') ?>">CreateNew Folder</a></p>
            <?php endif ?>
            </div>
        </div>                              
        <div class="form-group">
            <label for="" class="col-sm-4">Choose File</label>
            <div class="col-sm-8 tm-nopadding" style="padding-top: 5px;">
                <input type="file" name="userfile">
                    <div style="padding-left: 15px;">
                    <?php
                    if(substr($form[$lang->shortname]->type,0,5) == 'image') {
                        echo '<img src="'.site_url('files/media') . '/' . $form[$lang->shortname]->alias .'" alt="" class="img-responsive img-thumbnail" width="150" /><br><br>' . $form[$lang->shortname]->filename;
                    } else {
                        echo '<img src="'.base_url().'../assets/admin/ionicons/ios7-copy-outline.png" class="img-responsive img-thumbnail no-border no-padding" width="28"><br>'. $form[$lang->shortname]->filename;
                    }
                    ?>
                    <div><em><small class="helper">( The current file will be deleted immediately after a new file uploaded. )</small></em></div>
                    </div>
            </div>            
        </div>
    </div>
    <div class="panel-footer text-right">
        <input type="hidden" name="id" value="<?php echo $form[$lang->shortname]->id ?> ">
        <a href="<?php echo site_url('files/delete' . '/'. $form[$lang->shortname]->id) ?>" class="btn tm-btn delete" style="background-color: #DD4B39">Remove</a>
        <a href="<?php echo site_url('files') ?>" class="btn tm-btn">Cancel</a>
        <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>
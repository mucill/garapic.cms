<?php echo form_open(site_url('/auth/create_user'), 'class="form-horizontal" role="form"');?>
    <div id="infoMessage"><?php echo $message;?></div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Profile of <?php echo $first_name['value'] ?> <?php echo $last_name['value'] ?></h3>
        </div>
        <div class="box-body tm-padding">            
            <div class="form-group">
                <label for="" class="col-sm-4">First Name</label>
                <div class="col-sm-8">
                <?php echo form_input($first_name,'','class="form-control"');?>
                </div>
            </div>
                 
            <div class="form-group">
                <label for="" class="col-sm-4">Last Name</label>
                <div class="col-sm-8">
                <?php echo form_input($last_name,'','class="form-control"');?>
                </div>
            </div>                                 
            <div class="form-group">
                <label for="" class="col-sm-4">Departmen</label>
                <div class="col-sm-8">
                <?php echo form_input($company,'','class="form-control"');?>
                </div>
            </div>                                 
            <div class="form-group">
                <label for="" class="col-sm-4">Phone</label>
                <div class="col-sm-8">
                <?php echo form_input($phone,'','class="form-control"');?>
                </div>
            </div>    
    </div>
    </div>
    <div class="box">
        <div class="box-header with-border">
            <h1 class="box-title">Authentication
            <br>
            <small>Please complete the form below to identified your login information.</small>
            </h1>
        </div>
        <div class="box-body tm-padding">
            <div class="form-group">
                <label for="" class="col-sm-4">Email</label>
                <div class="col-sm-8">
                <?php echo form_input($email,'','class="form-control"');?>
                <small class="help-block">Use as Login ID. Cannot be change after registration. </small>
                </div>
            </div>                                 
            <?php if($identity_column!=='email') : ?>
            <div class="form-group">
                <label for="" class="col-sm-4">Username</label>
                <div class="col-sm-8">
                    <?php echo form_input($identity,'','class="form-control"') ?>
                </div>
            </div>                                 
            <?php endif ?>
            <div class="form-group">
                <label for="" class="col-sm-4">New Password</label>
                <div class="col-sm-8">
                <?php echo form_input($password,'','class="form-control"');?>
                <small class="help-block">min. 8 characters</small>
                </div>
            </div>                                 
            <div class="form-group">
                <label for="" class="col-sm-4">Retype New Password</label>
                <div class="col-sm-8">
                <?php echo form_input($password_confirm,'','class="form-control"');?>
                </div>
            </div>                                 
        </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('members') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
    </div>

<?php echo form_close();?>
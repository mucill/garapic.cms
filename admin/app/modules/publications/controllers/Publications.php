<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publications extends MX_Controller {

    private $table_name     = 'member_publikasi';
    private $url            = 'publications';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';
    public $pub_type       = array('Jurnal', 'Proceding', 'Buku', 'Lainnya');

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // load library
        $this->load->library('upload');
        $this->load->helper('text');

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Publications';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    // List all your items
    public function index( $offset = 0 )
    {

        // pagination
        $this->db
        ->select('pub_id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->from($this->table_name)
        ->order_by('last_update','desc')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {                   
                /**
                 * Add an action button for non member only.
                 * This action to avoid an unexpected behavior such as updated or deleted by non-member.
                 */
                $this->update = '';
                $this->delete = '<small style="color: red;">No authority</small>';
                if($list->member_id <= 2 ) {
                    // check for update access
                    if($this->menu->crud_access('update')) {
                        $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->pub_id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                    }

                    // check for delete access
                    if($this->menu->crud_access('delete')) {
                        $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->pub_id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                    }
                }

                // return to table data
                $table_row = array(
                    ucwords(strtolower($list->pub_title)),
                    $list->pub_partner,
                    ($list->pub_file == '') ? '' : '<a href="'.REPOSITORY . DIRECTORY_SEPARATOR . $list->pub_file .'">Download</a>',
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Title', 'Author', 'Download', '');
            } else {
                $this->table->set_heading('Title', 'Author', 'Download');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // additional info
        $this->data['scripts'] = '
        $(".content").prepend("<div class=\"well well-lg no-shadow\">Please note that you have no authority to change member\'s publications except it uploaded by backend user. </div>");
        ';
        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // validation rules
        $this->form_validation->set_rules("title", "Title", 'required');
        $this->form_validation->set_rules("abstract", "Abstract", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {

            $this->data['content'] = $this->load->view('add', $this->data, true);

        } else {

            // for uploaded
            $file_name      = '';
            if (!empty($_FILES['userfile']['name'])) {
                $path_parts = pathinfo($_FILES["userfile"]["name"]);
                $extension  = $path_parts['extension'];
                $config['upload_path']          = REPOSITORY;
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|csv';
                $config['file_name']            = $_FILES['userfile']['name'];
                $config['remove_spaces']        = FALSE;
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $metafile                       = $this->upload->data();
                $file_name                      = $metafile["file_name"];
            }

            // save 
            $record = array(
                'member_id'     => 0, // mean it's uploaded by Admin
                'pub_title'     => $this->input->post('title'),
                'pub_partner'   => $this->input->post('author'),
                'pub_type'      => $this->input->post('type'),
                'pub_isbn'      => $this->input->post('isbn'),
                'pub_year'      => $this->input->post('year'),
                'pub_abstract'  => $this->input->post('abstract'),
                'pub_link'      => $this->input->post('link'),
                'pub_file'      => $file_name,
                'last_update'   => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }
        // make an output
        $this->load->view('admin/content', $this->data);        

    }

    // Update one item
    public function update( $id = NULL )
    {
        // check for write access
        $this->menu->check_access('update');

        // get current data 
        $this->db->where('pub_id', $id);
        $query = $this->db->get($this->table_name);
        if($query->num_rows() > 0 ) {
            $this->data['form'] = $query->row();        
        }

        // validation rules
        $this->form_validation->set_rules("title", "Title", 'required');
        $this->form_validation->set_rules("abstract", "Abstract", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {

            $this->data['content'] = $this->load->view('edit', $this->data, true);

        } else {

            // for uploaded
            $file_name      =  $this->data['form']->pub_file;
            if (!empty($_FILES['userfile']['name'])) {

                if(file_exists(REPOSITORY . DIRECTORY_SEPARATOR . $this->data['form']->pub_file)) {
                    @unlink(REPOSITORY . DIRECTORY_SEPARATOR .$this->data['form']->pub_file);
                }

                $path_parts                     = pathinfo($_FILES["userfile"]["name"]);
                $extension                      = $path_parts['extension'];
                $config['upload_path']          = REPOSITORY;
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|csv';
                $config['file_name']            = $_FILES['userfile']['name'];
                $config['remove_spaces']        = FALSE;
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $metafile                       = $this->upload->data();
                $file_name                      = $metafile["file_name"];
            }

            // save 
            $record = array(
                'member_id'     => 0, // mean it's uploaded by Admin
                'pub_title'     => $this->input->post('title'),
                'pub_partner'   => $this->input->post('author'),
                'pub_type'      => $this->input->post('type'),
                'pub_isbn'      => $this->input->post('isbn'),
                'pub_year'      => $this->input->post('year'),
                'pub_abstract'  => $this->input->post('abstract'),
                'pub_link'      => $this->input->post('link'),
                'pub_file'      => $file_name,
                'last_update'   => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->where('pub_id', $this->input->post('id'));
            $this->db->update($this->table_name, $record);

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('edit', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }
        // make an output
        $this->load->view('admin/content', $this->data);        

    }

    // Delete one item
    public function delete( $id = NULL )
    {

        // security check
        $this->menu->check_access('delete');

        // get current data
        $this->db
        ->where('pub_id', $id);
        $query  = $this->db->get($this->table_name);
        $file   = $query->row(); 

        $this->db->trans_start();
        $this->db->where('pub_id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            // let's remove it
            $message = 'Your record has been removed.';
            @unlink(REPOSITORY . '/'. $file->pub_file);
        }
        $this->data['button_link']      = site_url($this->url);
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }
}

/* End of file Publications.php */
/* Location: .//D/Webs/mgi/admin/app/modules/proceeding/controllers/Publications.php */

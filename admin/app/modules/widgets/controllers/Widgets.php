<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Widgets extends MX_Controller {

    private $table_name     = 'cms_widget';
    private $url            = 'widgets';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies

        // for security check
        $this->menu->has_access();
        
        // need to manipulate url title
        $this->load->helper('url');
        
        // for header
        $this->data                 = $this->header->button_header();
        $this->data['title']        = 'Widgets';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    // List all your items
    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->from($this->table_name)
        ->group_by('id')
        ->order_by('slug')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';                    
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                    $list->name,
                    $list->slug,
                    $list->updated_at
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Namae', 'Alias', 'Last Update', '');
            } else {
                $this->table->set_heading('Namae', 'Alias', 'Last Update' );
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // validation
        $this->form_validation->set_rules("name", "Widget's Name", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('add', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'name'          => $this->input->post('name'),
                'content'       => $this->input->post('content'),
                'created_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
             $id = $this->db->insert_id();

            // update slug by id
            $record = array(
                'slug' => 'widget-'.$id
            );
            $this->db->where('id', $id);
            $this->db->update($this->table_name, $record);

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        $this->data['js_files'] = array(
            base_url() . PLUGINS. 'ace/noconflict/ace.js',
            base_url() . PLUGINS. 'ace/noconflict/ext-textarea.js'
        );

        $this->data['scripts'] = '
            var editor = ace.edit("editor");
                editor.setTheme("ace/theme/chrome");
                editor.session.setMode("ace/mode/html");
                editor.session.setMode("ace/mode/php");
                editor.setAutoScrollEditorIntoView(true);
                editor.setOption("minLines", 20);
                editor.setOption("maxLines", 20);
                editor.getSession().on("change", function(){ 
                    $("#content").val(editor.getSession().getValue()); 
                });
        ';

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Update one item
    public function update( $id = NULL )
    {
        // check for write access
        $this->menu->check_access('update');

        // get current data
        $query = $this->db->get_where($this->table_name, array('id' => $id));
        $this->data['form'] = $query->row();

        // validation
        $this->form_validation->set_rules("name", "Widget's Name", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('edit', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'name'          => $this->input->post('name'),
                'content'       => $this->input->post('content'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $record);

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('edit', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        $this->data['js_files'] = array(
            base_url() . PLUGINS. 'ace/noconflict/ace.js',
            base_url() . PLUGINS. 'ace/noconflict/ext-textarea.js'
        );

        $this->data['scripts'] = '
            var editor = ace.edit("editor");
                editor.setTheme("ace/theme/chrome");
                editor.session.setMode("ace/mode/html");
                editor.session.setMode("ace/mode/php");
                editor.setAutoScrollEditorIntoView(true);
                editor.setOption("minLines", 20);
                editor.setOption("maxLines", 20);
                editor.getSession().on("change", function(){ 
                    $("#content").val(editor.getSession().getValue()); 
                });
        ';

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->data['button_link'] = site_url($this->url);

        $this->db->trans_start();
        $this->db->delete($this->table_name, array('id' => $id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }
}

/* End of file widgets.php */
/* Location: .//D/Webs/mgi/admin/app/modules/widgets/controllers/widgets.php */

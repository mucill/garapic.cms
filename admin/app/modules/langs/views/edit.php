<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">
        <div class="form-group">
            <label class="col-sm-4">Name</label>
            <div class="col-sm-8">
            <?php echo form_input('langs', set_value('langs', $form->name), 'class="form-control" placeholder="Ex. English, Bahasa Indonesia"') ?>
            </div>
        </div>              
        <div class="form-group">
            <label class="col-sm-4">Shortname</label>
            <div class="col-sm-8">
            <?php echo form_input('short', set_value('short', $form->shortname), 'class="form-control" placeholder="Ex. en, id, in, es"') ?>
            </div>
        </div>              
        <div class="form-group">
            <label for="" class="col-sm-4">Set as default</label>
            <div class="col-sm-8">
                <input type="checkbox" name="default" value="1" <?php echo ($form->is_default == 1) ? 'checked' : '' ?>>
            </div>
        </div>                              
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('langs') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>
<?php if(isset($_POST['q']) || $this->uri->total_segments() > 3 ) : ?>
    <p class="well no-shadow">
    <a href="<?php echo site_url($this->uri->segment(1)) ?>" class="btn-link pull-right">Clear Filter</a>
    Found(s) <strong><?php echo $count ?></strong> data for <strong class="text-red"><?php echo ($this->uri->segment(3) != null) ? $this->uri->segment(3) : $_POST['q'] ?></strong>
    </p>
<?php endif ?>
<?php if(count($list) > 0) : ?>
    <div class="box tm-padding">
        <div class="box-body no-padding">            
            <?php echo $list ?>
        </div>
        <div class="box-footer tm-padding tm-nopadding-right tm-nopadding-bottom text-right">
            <?php echo $pagination ?>
        </div>
    </div>
<?php else : ?>
<?php $this->load->view('admin/blank'); ?>
<?php endif ?>
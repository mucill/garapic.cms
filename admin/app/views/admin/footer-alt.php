      <!-- Main Footer -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          PT. Pendidikan Maritim dan Logistik Indonesia
        </div>
        <strong>Copyright &copy; 2016 <a href="#">PT.PMLI</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    <script src="<?php echo base_url().PLUGINS ?>print-area/demo/jquery.PrintArea.js"></script>

    <script src="<?php echo base_url().PLUGINS ?>bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().PLUGINS ?>colorbox/jquery.colorbox-min.js"></script>
    <script src="<?php echo base_url().ADM_JS ?>app.min.js"></script>
    <?php if(isset($js_files)):
    foreach($js_files as $file): ?><script src="<?php echo $file; ?>"></script>
    <?php endforeach; endif; ?>  
    <?php if(isset($scripts)): ?>
    <script type="text/javascript"><?php echo $scripts; ?></script>
    <?php endif; ?>  
    <script src="<?php echo base_url().ADM_JS ?>custom.js"></script>
<?php if($this->uri->segment(1) == '' || $this->uri->segment(1) == 'dashboard' ) : ?>
<script>
(function($) {

  "use strict";

  var options = {
    events_source: '<?php echo site_url('dashboard/calendar_events') ?>',
    view: 'month',
    tmpl_path: '<?php echo base_url() .PLUGINS ?>bootstrap-calendar/tmpls/',
    tmpl_cache: false,
    day: '<?php echo date("Y-m-d") ?>',
    onAfterEventsLoad: function(events) {
      if(!events) {
        return;
      }
      var list = $('#eventlist');
      list.html('');

      $.each(events, function(key, val) {
        $(document.createElement('li'))
          .html('<a href="' + val.url + '">' + val.title + '</a>')
          .appendTo(list);
      });
    },
    onAfterViewLoad: function(view) {
      $('.page-header h3').text(this.getTitle());
      $('.btn-group button').removeClass('active');
      $('button[data-calendar-view="' + view + '"]').addClass('active');
    },
    classes: {
      months: {
        general: 'label'
      }
    }
  };

  var calendar = $('#calendar').calendar(options);

  $('.btn-group button[data-calendar-nav]').each(function() {
    var $this = $(this);
    $this.click(function() {
      calendar.navigate($this.data('calendar-nav'));
    });
  });

  $('.btn-group button[data-calendar-view]').each(function() {
    var $this = $(this);
    $this.click(function() {
      calendar.view($this.data('calendar-view'));
    });
  });

  $('#first_day').change(function(){
    var value = $(this).val();
    value = value.length ? parseInt(value) : null;
    calendar.setOptions({first_day: value});
    calendar.view();
  });

  $('#language').change(function(){
    calendar.setLanguage($(this).val());
    calendar.view();
  });

  $('#events-in-modal').change(function(){
    var val = $(this).is(':checked') ? $(this).val() : null;
    calendar.setOptions({modal: val});
  });
  $('#format-12-hours').change(function(){
    var val = $(this).is(':checked') ? true : false;
    calendar.setOptions({format12: val});
    calendar.view();
  });
  $('#show_wbn').change(function(){
    var val = $(this).is(':checked') ? true : false;
    calendar.setOptions({display_week_numbers: val});
    calendar.view();
  });
  $('#show_wb').change(function(){
    var val = $(this).is(':checked') ? true : false;
    calendar.setOptions({weekbox: val});
    calendar.view();
  });
  $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
    //e.preventDefault();
    //e.stopPropagation();
  });
}(jQuery));
</script>
<?php endif ?>
  </body>
</html>
